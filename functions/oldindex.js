const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });
const admin = require('firebase-admin');
const linear16 = require('linear16');
//admin.initializeApp();
admin.initializeApp(functions.config().firebase);

exports.transcrever = functions.https.onCall(content => {
  return transcrever(content.arq, content.type, content.user, content.idioma);
});

async function transcrever(arq, type, user, idioma) {
  console.log('Transcrever', arq, idioma, type, user);

  const gcsUri = arq;
  const languageCode = idioma;
  const speech = require('@google-cloud/speech');
  const client = new speech.SpeechClient();

  let config = {};
  if (type === 'audio/wav') {
    config = {
      // WAV
      encoding: 'LINEAR16',
      languageCode: languageCode
    };
  } else if (type === 'audio/x-m4a') {
    config = {
      // m4a
      encoding: 'LINEAR16', // FLAC
      languageCode: languageCode,
      sampleRateHertz: 16000
      //audioChannelCount: 2,
      //enableSeparateRecognitionPerChannel: true
    };
  }

  const audio = {
    uri: gcsUri
  };
  const request = {
    config: config,
    audio: audio
  };

  const [operation] = await client.longRunningRecognize(request);
  const [response] = await operation.promise();
  const transcription = response.results
    .map(result => result.alternatives[0].transcript)
    .join('\n');

  gravarTranscricao(user, transcription);
  return transcription;
}

async function gravarTranscricao(user, transcription) {
  console.log('Texto gravando: ', transcription);
}
