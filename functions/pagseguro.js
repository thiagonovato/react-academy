const request = require('request-promise');
const parse = require('xml2js').parseString;

const email = 'thiagonovato@gmail.com';
const token =
  'c2b5c6d6-09f5-4c47-99e1-de109d0dbfab07142642419497a58fc31ace88e5af984010-9cad-458a-b598-59a5d5383757';

/*
  // checkout
request({
  uri: 'https://ws.pagseguro.uol.com.br//v2/checkout',
  method: 'POST',
  form: {
    token: token,
    email: email,
    currency: 'BRL',
    itemId1: 'id',
    itemDescription1: 'nome',
    itemQuantity1: '1',
    itemAmount1: '2.00'
  },
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  }
}).then(data => {
  parse(data, (err, json) => {
    console.log(json.checkout.code[0]);
  });
});
*/

const notificationCode = '2AE820C8-A7BE-48EA-B656-F2D72A0675C6';
const consultaNotificacao =
  'https://ws.pagseguro.uol.com.br/v3/transactions/notifications/';
request(
  consultaNotificacao + notificationCode + '?token=' + token + '&email=' + email
).then(notificationXML => {
  parse(notificationXML, (err, transactionJSON) => {
    const transaction = transactionJSON.transaction;
    const status = transaction.status[0];
    const amount = transaction.grossAmount[0];
  });
});
