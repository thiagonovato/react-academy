const functions = require('firebase-functions');
const admin = require('firebase-admin');

const axios = require('axios');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

// Libs conversão para flac
const gcs = require('@google-cloud/storage')();
const path = require('path');
const os = require('os');
const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const ffmpeg_static = require('ffmpeg-static');

// Chave de autenticação
var serviceAccount = require('./academy-val-firebase-adminsdk-uapvn-92897ee86f.json');

// Resolvendo o problema de ECONNRESET
gcs.interceptors.push({
  request: function(reqOpts) {
    reqOpts.forever = false;
    return reqOpts;
  }
});
// FIM Resolvendo o problema de ECONNRESET

app.use(cors());
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://academy-val.firebaseio.com'
});

const request = require('request-promise');
const parse = require('xml2js').parseString;

// Dados para o pagseguro
const email = 'thiagonovato@gmail.com';

//produção
const uri = 'https://ws.pagseguro.uol.com.br//v2/checkout';
const token =
  'c2b5c6d6-09f5-4c47-99e1-de109d0dbfab07142642419497a58fc31ace88e5af984010-9cad-458a-b598-59a5d5383757';
const checkoutUrl =
  'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=';
const consultaNotificacao =
  'https://ws.pagseguro.uol.com.br/v3/transactions/notifications/';

// sandbox
/*
const uri = 'https://ws.sandbox.pagseguro.uol.com.br//v2/checkout';
const token = '63B78428A95E4077B77FC85B508D0788';
const checkoutUrl =
  'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=';

const consultaNotificacao =
  'https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/notifications/';
*/

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send('Academy Server');
});

app.post('/pay', (req, res) => {
  const { idUser, id, nome, valor } = req.body;
  request({
    uri: uri,
    method: 'POST',
    form: {
      token: token,
      email: email,
      currency: 'BRL',
      itemId1: idUser + '-' + id,
      itemDescription1: 'Transcrição do arquivo ' + nome,
      itemQuantity1: '1',
      itemAmount1: valor
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  }).then(data => {
    parse(data, (err, json) => {
      res.send({
        url: checkoutUrl + json.checkout.code[0]
      });
    });
  });
});

app.post('/confirmedPayment', (req, res) => {
  const notificationCode = req.body.notificationCode;
  console.log('Recebido POST do PagSeguro:', notificationCode);
  request(
    consultaNotificacao +
      notificationCode +
      '?token=' +
      token +
      '&email=' +
      email
  ).then(notificationXML => {
    parse(notificationXML, (err, transactionJSON) => {
      const transaction = transactionJSON.transaction;
      const status = transaction.status[0];

      const id = transaction.items[0].item[0].id[0];
      const idDivisor = id.split('-');
      const idUser = idDivisor[0];
      const idFile = idDivisor[1];

      // Pegando todos os dados do arquivo
      admin
        .firestore()
        .collection('academy')
        .doc(idUser)
        .collection('arquivos')
        .doc(idFile)
        .get()
        .then(doc => {
          file = doc.data();
          // Salvando no banco de dados
          admin
            .firestore()
            .collection('academy')
            .doc(idUser)
            .collection('arquivos')
            .doc(idFile)
            .set({
              ...file,
              statusPayment: status
            });
          // Fim salvando no banco de dados

          // Disparar a transcrição caso o status seja 5 (pago)
          console.log('Status da transcrição:', status);
          if (status === '3') {
            console.log(
              'Enviando pedido de transcrição.',
              notificationCode,
              file.flac,
              file.outputLanguage
            );
            axios
              .post(
                'https://speech.googleapis.com/v1/speech:longrunningrecognize?key=AIzaSyBca3CawBtPSd0a13pzQ6ZTSQVvWPB0m7c',
                {
                  config: {
                    enableAutomaticPunctuation: true,
                    languageCode: file.outputLanguage,
                    audioChannelCount: 1
                  },
                  audio: {
                    uri: file.flac
                  }
                }
              )
              .then(result => {
                console.log('Axios OK');
                admin
                  .firestore()
                  .collection('academy')
                  .doc(idUser)
                  .collection('arquivos')
                  .doc(idFile)
                  .set({
                    ...file,
                    statusPayment: status,
                    status: 'Transcrevendo',
                    codgTranscricao: result.data.name,
                    progressPercent: '0'
                  });
                getStatusTranscription(idUser, idFile, result.data.name);
              })
              .catch(err => {
                console.log('Axios ERRO', err.response);
              });
            // Fim da solicitação da transcrição
          }
          // FIM DO CÓDIGO 3 - PAGO
        });
      // Fim pegando todos os dados do arquivo
    });
  });
  console.log('Pedido OK. Aguardando transcrição.');
  res.send('ok');
});

async function getStatusTranscription(idUser, idFile, codg) {
  for (var i = 0; i < 540; i++) {
    console.log('Volta:', i);
    axios
      .get(
        `https://speech.googleapis.com/v1p1beta1/operations/${codg}?key=AIzaSyBca3CawBtPSd0a13pzQ6ZTSQVvWPB0m7c`
      )
      .then(response => {
        // Tem progresso?
        if (response.data.metadata.progressPercent !== undefined) {
          admin
            .firestore()
            .collection('academy')
            .doc(idUser)
            .collection('arquivos')
            .doc(idFile)
            .get()
            .then(doc => {
              file = doc.data();
              admin
                .firestore()
                .collection('academy')
                .doc(idUser)
                .collection('arquivos')
                .doc(idFile)
                .set({
                  ...file,
                  progressPercent: response.data.metadata.progressPercent,
                  status:
                    response.data.metadata.progressPercent === 100
                      ? 'Transcrito'
                      : 'Transcrevendo'
                });
            });
          if (response.data.metadata.progressPercent === 100) {
            i = 540;
            // Tratando o resultado
            let transcricao = '';
            response.data.response.results.map(i => {
              i.alternatives.map(a => {
                transcricao = transcricao + a.transcript;
              });
            });
            // Fim tratando o resultado
            // Criar novo arquivo
            console.log('Gravar arquivo.');
            admin
              .firestore()
              .collection('academy')
              .doc(idUser)
              .collection('documentos')
              .add({
                titulo: file.nome,
                texto: transcricao
              })
              .then(resultDocumento => {
                admin
                  .firestore()
                  .collection('academy')
                  .doc(idUser)
                  .collection('arquivos')
                  .doc(idFile)
                  .set({
                    ...file,
                    status: 'Transcrito',
                    linkDocumento: resultDocumento.id
                  });
              });
            // Fim criar novo arquivo
          }
        } // FIM TEM PROGRESSO
      })
      .catch(err => {
        console.log('Deu erro.');
      });
    if (i === 540) break;
    await sleep(5000);
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// ********************************************************************
// Makes an ffmpeg command return a promise.
function promisifyCommand(command) {
  return new Promise((resolve, reject) => {
    command
      .on('end', resolve)
      .on('error', reject)
      .run();
  });
}

/**
 * When an audio is uploaded in the Storage bucket We generate a mono channel audio automatically using
 * node-fluent-ffmpeg.
 */
exports.generateMonoAudio = functions.storage
  .object()
  .onFinalize(async object => {
    const fileBucket = object.bucket; // The Storage bucket that contains the file.
    const filePath = object.name; // File path in the bucket.
    const contentType = object.contentType; // File content type.

    // Exit if this is triggered on a file that is not an audio.
    if (!contentType.startsWith('audio/')) {
      console.log('This is not an audio.');
      return null;
    }

    // Get the file name.
    const fileName = path.basename(filePath);
    // Exit if the audio is already converted.
    if (fileName.endsWith('_output.flac')) {
      console.log('Already a converted audio.');
      return null;
    }

    // Download file from bucket.
    const bucket = gcs.bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);
    // We add a '_output.flac' suffix to target audio file name. That's where we'll upload the converted audio.
    const targetTempFileName =
      fileName.replace(/\.[^/.]+$/, '') + '_output.flac';
    const targetTempFilePath = path.join(os.tmpdir(), targetTempFileName);
    const targetStorageFilePath = path.join(
      path.dirname(filePath),
      targetTempFileName
    );

    await bucket.file(filePath).download({ destination: tempFilePath });
    console.log('Audio downloaded locally to', tempFilePath);
    // Convert the audio to mono channel using FFMPEG.

    let command = ffmpeg(tempFilePath)
      .setFfmpegPath(ffmpeg_static.path)
      .audioChannels(1)
      .audioFrequency(16000)
      .format('flac')
      .output(targetTempFilePath);

    await promisifyCommand(command);
    console.log('Output audio created at', targetTempFilePath);
    // Uploading the audio.
    await bucket.upload(targetTempFilePath, {
      destination: targetStorageFilePath
    });
    console.log('Output audio uploaded to', targetStorageFilePath);

    // Once the audio has been uploaded delete the local file to free up disk space.
    fs.unlinkSync(tempFilePath);
    fs.unlinkSync(targetTempFilePath);

    return console.log('Temporary files removed.', targetTempFilePath);
  });

exports.api = functions.https.onRequest(app);
