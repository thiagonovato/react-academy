import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { auth } from './configs/Firebase'

ReactDOM.render(<App auth={auth} />, document.getElementById('root'));

serviceWorker.unregister();
