import * as authActions from './auth';
import * as categorias from './categorias'
import * as documentos from './documentos'
import * as upload from './upload';

export default Object.assign({},
    { ...authActions },
    { ...categorias },
    { ...documentos },
    { ...upload }
    // Aqui abaixo, você vai adicionando as outras actions
    // ...
);
