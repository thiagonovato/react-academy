import { db } from '../configs/Firebase';
import {
  LOAD_DOCUMENTOS_PENDING,
  LOAD_DOCUMENTOS_ERROR,
  LOAD_DOCUMENTOS_SUCCESS,
  LOAD_DOCUMENTOS_DATA,
  SAVE_DOCUMENTOS_PENDING,
  SAVE_DOCUMENTOS_ERROR,
  SAVE_DOCUMENTOS_SUCCESS,
  LOAD_DOCUMENTO_PENDING,
  LOAD_DOCUMENTO_ERROR,
  LOAD_DOCUMENTO_SUCCESS,
  LOAD_DOCUMENTO_DATA,
  UPDATE_DOCUMENTO_PENDING,
  UPDATE_DOCUMENTO_ERROR,
  UPDATE_DOCUMENTO_SUCCESS
} from './types';

// Carregando documentos
function setDocumentoPending(isDocumentoPending) {
  return { type: LOAD_DOCUMENTOS_PENDING, isDocumentoPending };
}

function setDocumentoErro(isDocumentoErro) {
  return { type: LOAD_DOCUMENTOS_ERROR, isDocumentoErro };
}

function setDocumentoSuccess(isDocumentoSuccess) {
  return { type: LOAD_DOCUMENTOS_SUCCESS, isDocumentoSuccess };
}

function setDocumentoData(documentoData) {
  return { type: LOAD_DOCUMENTOS_DATA, documentoData };
}

// Salvando documento
function setCreateDocumentoPending(isCreateDocumentoPending) {
  return { type: SAVE_DOCUMENTOS_PENDING, isCreateDocumentoPending };
}

function setCreateDocumentoErro(isCreateDocumentoErro) {
  return { type: SAVE_DOCUMENTOS_ERROR, isCreateDocumentoErro };
}

function setCreateDocumentoSuccess(isCreateDocumentoSuccess) {
  return { type: SAVE_DOCUMENTOS_SUCCESS, isCreateDocumentoSuccess };
}

// Buscando documento
function setSearchDocumentoPending(isSearchDocumentoPending) {
  return { type: LOAD_DOCUMENTO_PENDING, isSearchDocumentoPending };
}

function setSearchDocumentoErro(isSearchDocumentoErro) {
  return { type: LOAD_DOCUMENTO_ERROR, isSearchDocumentoErro };
}

function setSearchDocumentoSuccess(isSearchDocumentoSuccess) {
  return { type: LOAD_DOCUMENTO_SUCCESS, isSearchDocumentoSuccess };
}

function setSearchDocumentoData(documentoSearchData) {
  return { type: LOAD_DOCUMENTO_DATA, documentoSearchData };
}

// Update documento
function setUpdateDocumentoPending(isUpdateDocumentoPending) {
  return { type: UPDATE_DOCUMENTO_PENDING, isUpdateDocumentoPending };
}

function setUpdateDocumentoErro(isUpdateDocumentoErro) {
  return { type: UPDATE_DOCUMENTO_ERROR, isUpdateDocumentoErro };
}

function setUpdateDocumentoSuccess(isUpdateDocumentoSuccess) {
  return { type: UPDATE_DOCUMENTO_SUCCESS, isUpdateDocumentoSuccess };
}

export function listarDocumentos() {
  return dispatch => {
    dispatch(setDocumentoPending(true));
    dispatch(setDocumentoErro(false));
    dispatch(setDocumentoSuccess(false));

    dispatch(setCreateDocumentoPending(false));
    dispatch(setCreateDocumentoErro(false));
    dispatch(setCreateDocumentoSuccess(false));

    dispatch(setUpdateDocumentoPending(false));
    dispatch(setUpdateDocumentoErro(false));
    dispatch(setUpdateDocumentoSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');

    db.collection('academy')
      .doc(idUser)
      .collection('documentos')
      .onSnapshot(
        categorias => {
          let data = [];
          categorias.forEach(key => {
            data.push({
              id: key.id,
              ...key.data()
            });
          });
          dispatch(setDocumentoPending(false));
          dispatch(setDocumentoSuccess(true));
          dispatch(setDocumentoData(data));
        },
        error => {
          dispatch(setDocumentoPending(false));
          dispatch(setDocumentoSuccess(false));
          dispatch(setDocumentoErro(true));
        }
      );
  };
}

export function createDocumento(documento) {
  return dispatch => {
    dispatch(setCreateDocumentoPending(true));
    dispatch(setCreateDocumentoErro(false));
    dispatch(setCreateDocumentoSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');

    db.collection('academy')
      .doc(idUser)
      .collection('documentos')
      .add({
        titulo: documento.titulo,
        texto: documento.texto,
        categoria: documento.categoria
      })
      .then(function(docRef) {
        dispatch(setCreateDocumentoPending(false));
        dispatch(setCreateDocumentoSuccess(true));
      })
      .catch(function(error) {
        dispatch(setCreateDocumentoPending(false));
        dispatch(setCreateDocumentoErro(true));
      });
  };
}

export function deleteDocumento(id) {
  return dispatch => {
    dispatch(setDocumentoPending(true));
    dispatch(setDocumentoSuccess(false));
    dispatch(setDocumentoErro(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('documentos')
      .doc(id)
      .delete()
      .then(() => {
        dispatch(setDocumentoPending(false));
        dispatch(setDocumentoSuccess(true));
        dispatch(listarDocumentos());
      })
      .catch(error => {
        dispatch(setDocumentoPending(false));
        dispatch(setDocumentoErro(true));
      });
  };
}

export function buscarDocumento(id) {
  return dispatch => {
    dispatch(setSearchDocumentoPending(true));
    dispatch(setSearchDocumentoErro(false));
    dispatch(setSearchDocumentoSuccess(false));

    dispatch(setUpdateDocumentoPending(false));
    dispatch(setUpdateDocumentoErro(false));
    dispatch(setUpdateDocumentoSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('documentos')
      .doc(id)
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log('Ok');
        } else {
          let documento = doc.data();
          dispatch(setSearchDocumentoData(documento));
          dispatch(setSearchDocumentoPending(false));
          dispatch(setSearchDocumentoSuccess(true));
        }
      });
  };
}

export function updateDocumento(documento) {
  return dispatch => {
    dispatch(setUpdateDocumentoPending(true));
    dispatch(setUpdateDocumentoErro(false));
    dispatch(setUpdateDocumentoSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('documentos')
      .doc(documento.id)
      .set({
        titulo: documento.titulo,
        texto: documento.texto,
        categoria: documento.categoria ? documento.categoria : ''
      })
      .then(() => {
        dispatch(setUpdateDocumentoPending(false));
        dispatch(setUpdateDocumentoSuccess(true));
      })
      .catch(error => {
        dispatch(setUpdateDocumentoPending(false));
        dispatch(setUpdateDocumentoErro(true));
      });
  };
}
