import { db } from '../configs/Firebase';
import {
  LOAD_CATEGORIAS_PENDING,
  LOAD_CATEGORIAS_ERROR,
  LOAD_CATEGORIAS_SUCCESS,
  LOAD_CATEGORIAS_DATA,
  SAVE_CATEGORIAS_PENDING,
  SAVE_CATEGORIAS_ERROR,
  SAVE_CATEGORIAS_SUCCESS,
  LOAD_CATEGORIA_PENDING,
  LOAD_CATEGORIA_ERROR,
  LOAD_CATEGORIA_SUCCESS,
  LOAD_CATEGORIA_DATA,
  UPDATE_CATEGORIA_PENDING,
  UPDATE_CATEGORIA_ERROR,
  UPDATE_CATEGORIA_SUCCESS
} from './types';

// Carregando categorias
function setCategoriaPending(isCategoriaPending) {
  return { type: LOAD_CATEGORIAS_PENDING, isCategoriaPending };
}

function setCategoriaErro(isCategoriaErro) {
  return { type: LOAD_CATEGORIAS_ERROR, isCategoriaErro };
}

function setCategoriaSuccess(isCategoriaSuccess) {
  return { type: LOAD_CATEGORIAS_SUCCESS, isCategoriaSuccess };
}

function setCategoriaData(categoriaData) {
  return { type: LOAD_CATEGORIAS_DATA, categoriaData };
}

// Salvando categoria
function setCreateCategoriaPending(isCreateCategoriaPending) {
  return { type: SAVE_CATEGORIAS_PENDING, isCreateCategoriaPending };
}

function setCreateCategoriaErro(isCreateCategoriaErro) {
  return { type: SAVE_CATEGORIAS_ERROR, isCreateCategoriaErro };
}

function setCreateCategoriaSuccess(isCreateCategoriaSuccess) {
  return { type: SAVE_CATEGORIAS_SUCCESS, isCreateCategoriaSuccess };
}

// Buscando categoria
function setSearchCategoriaPending(isSearchCategoriaPending) {
  return { type: LOAD_CATEGORIA_PENDING, isSearchCategoriaPending };
}

function setSearchCategoriaErro(isSearchCategoriaErro) {
  return { type: LOAD_CATEGORIA_ERROR, isSearchCategoriaErro };
}

function setSearchCategoriaSuccess(isSearchCategoriaSuccess) {
  return { type: LOAD_CATEGORIA_SUCCESS, isSearchCategoriaSuccess };
}

function setSearchCategoriaData(categoriaSearchData) {
  return { type: LOAD_CATEGORIA_DATA, categoriaSearchData };
}

// Update categoria
function setUpdateCategoriaPending(isUpdateCategoriaPending) {
  return { type: UPDATE_CATEGORIA_PENDING, isUpdateCategoriaPending };
}

function setUpdateCategoriaErro(isUpdateCategoriaErro) {
  return { type: UPDATE_CATEGORIA_ERROR, isUpdateCategoriaErro };
}

function setUpdateCategoriaSuccess(isUpdateCategoriaSuccess) {
  return { type: UPDATE_CATEGORIA_SUCCESS, isUpdateCategoriaSuccess };
}

export function listarCategorias() {
  return dispatch => {
    dispatch(setCategoriaPending(true));
    dispatch(setCategoriaErro(false));
    dispatch(setCategoriaSuccess(false));

    dispatch(setCreateCategoriaPending(false));
    dispatch(setCreateCategoriaErro(false));
    dispatch(setCreateCategoriaSuccess(false));

    dispatch(setUpdateCategoriaPending(false));
    dispatch(setUpdateCategoriaErro(false));
    dispatch(setUpdateCategoriaSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('categorias')
      .onSnapshot(
        categorias => {
          let data = [];
          categorias.forEach(key => {
            data.push({
              id: key.id,
              ...key.data()
            });
          });
          dispatch(setCategoriaPending(false));
          dispatch(setCategoriaSuccess(true));
          dispatch(setCategoriaData(data));
        },
        error => {
          dispatch(setCategoriaPending(false));
          dispatch(setCategoriaSuccess(false));
          dispatch(setCategoriaErro(true));
        }
      );
  };
}

export function createCategoria(nome) {
  return dispatch => {
    dispatch(setCreateCategoriaPending(true));
    dispatch(setCreateCategoriaErro(false));
    dispatch(setCreateCategoriaSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('categorias')
      .add({
        nome: nome
      })
      .then(function(docRef) {
        dispatch(setCreateCategoriaPending(false));
        dispatch(setCreateCategoriaSuccess(true));
      })
      .catch(function(error) {
        dispatch(setCreateCategoriaPending(false));
        dispatch(setCreateCategoriaErro(true));
      });
  };
}

export function deleteCategoria(id) {
  return dispatch => {
    dispatch(setCategoriaPending(true));
    dispatch(setCategoriaSuccess(false));
    dispatch(setCategoriaErro(false));

    const idUser = localStorage.getItem('@Academy:user');

    db.collection('academy')
      .doc(idUser)
      .collection('categorias')
      .doc(id)
      .delete()
      .then(() => {
        dispatch(setCategoriaPending(false));
        dispatch(setCategoriaSuccess(true));
        dispatch(listarCategorias());
      })
      .catch(error => {
        dispatch(setCategoriaPending(false));
        dispatch(setCategoriaErro(true));
      });
  };
}

export function buscarCategoria(id) {
  return dispatch => {
    dispatch(setSearchCategoriaPending(true));
    dispatch(setSearchCategoriaErro(false));
    dispatch(setSearchCategoriaSuccess(false));
    dispatch(setUpdateCategoriaPending(false));
    dispatch(setUpdateCategoriaErro(false));
    dispatch(setUpdateCategoriaSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');

    db.collection('academy')
      .doc(idUser)
      .collection('categorias')
      .doc(id)
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log('Ok');
        } else {
          let documento = doc.data();
          dispatch(setSearchCategoriaData(documento));
          dispatch(setSearchCategoriaPending(false));
          dispatch(setSearchCategoriaSuccess(true));
        }
      });
  };
}

export function updateCategoria(id, nome) {
  return dispatch => {
    dispatch(setUpdateCategoriaPending(true));
    dispatch(setUpdateCategoriaErro(false));
    dispatch(setUpdateCategoriaSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');

    db.collection('academy')
      .doc(idUser)
      .collection('categorias')
      .doc(id)
      .set({
        nome
      })
      .then(() => {
        dispatch(setUpdateCategoriaPending(false));
        dispatch(setUpdateCategoriaSuccess(true));
      })
      .catch(error => {
        dispatch(setUpdateCategoriaPending(false));
        dispatch(setUpdateCategoriaErro(true));
      });
  };
}
