import { storage, functions, db } from '../configs/Firebase';
import axios from 'axios';

import { Document, Packer, Paragraph } from 'docx';
import { saveAs } from 'file-saver';

import {
  UPLOAD_PENDING,
  UPLOAD_PERCENT,
  UPLOAD_ERROR,
  UPLOAD_SUCCESS,
  UPLOAD_CREATE_DOC_PENDING,
  UPLOAD_CREATE_DOC_SUCCESS,
  LOAD_ARQUIVOS_DATA,
  LOAD_ARQUIVOS_DATA_PENDING,
  LOAD_ARQUIVOS_DATA_SUCCESS,
  LOAD_ARQUIVOS_DATA_ERRO,
  DELETE_ARQUIVO_PENDING,
  DELETE_ARQUIVO_ERROR,
  DELETE_ARQUIVO_SUCCESS,
  STATUS_TRANSCREVENDO,
  PAY_PENDING,
  FILE_LOAD_PENDING,
  FILE_LOAD_SUCCESS,
  FILE_LOAD_ERROR,
  FILE_LOAD_DATA,
  FILE_SAVE_PENDING,
  FILE_SAVE_SUCCESS,
  FILE_SAVE_ERROR
} from './types';

function setUploadPending(isUploadPending) {
  return { type: UPLOAD_PENDING, isUploadPending };
}

function setUploadPercent(uploadPercent) {
  return { type: UPLOAD_PERCENT, uploadPercent };
}

function setUploadError(isUploadError) {
  return { type: UPLOAD_ERROR, isUploadError };
}

function setUploadSuccess(isUploadSuccess) {
  return { type: UPLOAD_SUCCESS, isUploadSuccess };
}

function setUploadCreateDocPending(isUploadCreateDocPending) {
  return { type: UPLOAD_CREATE_DOC_PENDING, isUploadCreateDocPending };
}

function setUploadCreateDocSuccess(isUploadCreateDocSuccess) {
  return { type: UPLOAD_CREATE_DOC_SUCCESS, isUploadCreateDocSuccess };
}

function setUploadData(uploadDataList) {
  return { type: LOAD_ARQUIVOS_DATA, uploadDataList };
}

function setUploadDataPending(isUploadDataPending) {
  return { type: LOAD_ARQUIVOS_DATA_PENDING, isUploadDataPending };
}

function setUploadDataSuccess(isUploadDataSuccess) {
  return { type: LOAD_ARQUIVOS_DATA_SUCCESS, isUploadDataSuccess };
}

function setUploadDataErro(isUploadDataErro) {
  return { type: LOAD_ARQUIVOS_DATA_ERRO, isUploadDataErro };
}

function setDeleteArquivoPending(isDeleteArquivoPending) {
  return { type: DELETE_ARQUIVO_PENDING, isDeleteArquivoPending };
}

function setDeleteArquivoError(isDeleteArquivoError) {
  return { type: DELETE_ARQUIVO_ERROR, isDeleteArquivoError };
}

function setDeleteArquivoSuccess(isDeleteArquivoSuccess) {
  return { type: DELETE_ARQUIVO_SUCCESS, isDeleteArquivoSuccess };
}

function setStatusTranscrevendo(isStatusTranscrevendo) {
  return { type: STATUS_TRANSCREVENDO, isStatusTranscrevendo };
}

function setPayPending(isPayPending) {
  return { type: PAY_PENDING, isPayPending };
}

// Buscando o arquivo
function setFileLoadPending(isFileLoadPending) {
  return { type: FILE_LOAD_PENDING, isFileLoadPending };
}
function setFileLoadSuccess(isFileLoadSuccess) {
  return { type: FILE_LOAD_SUCCESS, isFileLoadSuccess };
}
function setFileLoadError(isFileLoadError) {
  return { type: FILE_LOAD_ERROR, isFileLoadError };
}
function setFileLoadData(fileLoadData) {
  return { type: FILE_LOAD_DATA, fileLoadData };
}

// Salvando o arquivo
function setFileSavePending(isFileSavePending) {
  return { type: FILE_SAVE_PENDING, isFileSavePending };
}
function setFileSaveSuccess(isFileSaveSuccess) {
  return { type: FILE_SAVE_SUCCESS, isFileSaveSuccess };
}
function setFileSaveError(isFileSaveError) {
  return { type: FILE_SAVE_ERROR, isFileSaveError };
}

export function uploadFile(file) {
  return dispatch => {
    dispatch(setUploadPending(true));
    dispatch(setUploadPercent());
    dispatch(setUploadError(false));
    dispatch(setUploadSuccess(false));
    dispatch(setUploadCreateDocPending(false));
    dispatch(setUploadCreateDocSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');

    const { name, size, type } = file;

    // Definindo o nome do arquivo
    var today = new Date();
    var mes = today.getMonth() + 1;
    let nomeDiretorio = today.getFullYear() + '-' + mes + '-' + today.getDate();

    const ref = storage.ref(idUser + '/' + nomeDiretorio + '/' + name);

    let arquivo = {
      nome: name,
      size: size,
      type: type,
      data: nomeDiretorio
    };

    var uploadTask = ref.put(file);
    uploadTask.on(
      'state_changed',
      function(snapshot) {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        // Caso queira mostrar o progresso, descomentar a linha abaixo
        // SETSTATE PORCENTAGEM
        dispatch(setUploadPercent(progress));
      },
      function(error) {
        // Bloco de erro ao fazer o upload
        dispatch(setUploadError(true));
      },
      function() {
        // Bloco de sucesso ao fazer o upload
        dispatch(setUploadPending(false));
        dispatch(setUploadSuccess(true));

        // Bloco para registrar o novo documento
        dispatch(setUploadCreateDocPending(true));
        uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
          let novogStore =
            'gs://' +
            uploadTask.snapshot.ref.bucket +
            '/' +
            idUser +
            '/' +
            nomeDiretorio +
            '/' +
            name;

          let flacName =
            'gs://' +
            uploadTask.snapshot.ref.bucket +
            '/' +
            idUser +
            '/' +
            nomeDiretorio +
            '/' +
            name.replace(/\.[^/.]+$/, '') +
            '_output.flac';

          let novoArquivo = {
            // usando destructuring assignment, pegando o novoAnuncio e incrementando o atributo FOTO
            // o objetivo é que, usando img.metadata.downloadURLs[0] ficou obsoleto e não funciona.
            // A documentação do Firebase pede para usar desta forma
            ...arquivo,
            gStore: novogStore,
            link: downloadURL
          };

          // pegando o tempo do arquivo
          let fileB = URL.createObjectURL(file);
          var audioElement2 = new Audio(fileB);

          audioElement2.setAttribute('src', fileB);
          audioElement2.onloadedmetadata = function(e) {
            let tempoTotal = parseInt(this.duration);

            // fim pegando tempo do arquivo

            db.collection('academy')
              .doc(idUser)
              .collection('arquivos')
              .add({
                nome: novoArquivo.nome,
                size: novoArquivo.size,
                type: novoArquivo.type,
                gStore: novoArquivo.gStore,
                link: novoArquivo.link,
                data: novoArquivo.data,
                status: 'Não transcrito',
                comprimento: tempoTotal,
                statusPayment: '0',
                progressPercent: '0',
                flac: flacName,
                outputLanguage: 'pt-BR'
              })
              .then(function(docRef) {
                dispatch(setUploadCreateDocPending(false));
                dispatch(setUploadCreateDocSuccess(true));
              });
          };

          // Solicitando a conversão para flac
          axios
            .post('/api/flac', {
              idUser,
              nomeDiretorio,
              name,
              url: novoArquivo.link,
              gStore: novoArquivo.gStore
            })
            .then(result => {
              console.log('Deu certo', result);
            })
            .catch(e => {
              console.log('Deu erro,', e);
            });
        });
      }
    );
  };
}

export function listarArquivos() {
  return dispatch => {
    dispatch(setPayPending(false));

    dispatch(setUploadPending(false));
    dispatch(setUploadError(false));
    dispatch(setUploadSuccess(false));
    dispatch(setUploadCreateDocPending(false));
    dispatch(setUploadCreateDocSuccess(false));

    dispatch(setUploadDataPending(true));
    dispatch(setUploadDataSuccess(false));
    dispatch(setUploadDataErro(false));

    dispatch(setDeleteArquivoPending(false));
    dispatch(setDeleteArquivoSuccess(false));
    dispatch(setDeleteArquivoError(false));

    dispatch(setFileSaveSuccess(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('arquivos')
      .onSnapshot(
        categorias => {
          let data = [];
          categorias.forEach(key => {
            data.push({
              id: key.id,
              ...key.data()
            });
          });
          dispatch(setUploadDataPending(false));
          dispatch(setUploadDataSuccess(true));
          dispatch(setUploadData(data));
        },
        error => {
          dispatch(setUploadDataPending(false));
          dispatch(setUploadDataErro(true));
        }
      );
  };
}

export function deleteArquivo(arq) {
  return dispatch => {
    dispatch(setDeleteArquivoPending(true));
    dispatch(setDeleteArquivoSuccess(false));
    dispatch(setDeleteArquivoError(false));

    const idUser = localStorage.getItem('@Academy:user');

    const ref = storage.ref();
    var desertRef = ref.child(idUser + '/' + arq.data + '/' + arq.nome);

    db.collection('academy')
      .doc(idUser)
      .collection('arquivos')
      .doc(arq.id)
      .delete()
      .then(() => {
        dispatch(setDeleteArquivoPending(false));
        dispatch(setDeleteArquivoSuccess(true));
        dispatch(listarArquivos());
        desertRef
          .delete()
          .then(function() {})
          .catch(function(error) {
            // Uh-oh, an error occurred!
          });
      })
      .catch(error => {
        dispatch(setDeleteArquivoPending(false));
        dispatch(setDeleteArquivoError(true));
      });
  };
}

export function transcreveArquivo(arq, idioma) {
  return dispatch => {
    dispatch(setStatusTranscrevendo(true));
    // Setando variável
    const idUser = localStorage.getItem('@Academy:user');

    // Mudar seu status para TRANSCREVENDO
    db.collection('academy')
      .doc(idUser)
      .collection('arquivos')
      .doc(arq.id)
      .set({
        ...arq,
        status: 'Transcrevendo'
      });

    // Chama a function de transcrever
    var transcrever = functions.httpsCallable('transcrever');
    transcrever({
      arq: arq.gStore,
      type: arq.type,
      user: idUser,
      idioma: idioma
    })
      .then(function(result) {
        db.collection('academy')
          .doc(idUser)
          .collection('documentos')
          .add({
            titulo: arq.nome,
            texto: result.data
          });

        // Mudar seu status para TRANSCRITO
        db.collection('academy')
          .doc(idUser)
          .collection('arquivos')
          .doc(arq.id)
          .set({
            ...arq,
            status: 'Transcrito'
          });
        dispatch(setStatusTranscrevendo(false));
      })
      .catch(err => {
        db.collection('academy')
          .doc(idUser)
          .collection('arquivos')
          .doc(arq.id)
          .set({
            ...arq,
            status: 'Erro ao transcrever.'
          });
        dispatch(setStatusTranscrevendo(false));
      });
  };
}

export function pay(arq) {
  return dispatch => {
    dispatch(setPayPending(true));
    const idUser = localStorage.getItem('@Academy:user');
    // Calculando valor
    const valorMinimo = 1;
    const valor =
      arq.comprimento / 60 < 1
        ? valorMinimo.toFixed(2)
        : ((arq.comprimento / 60) * 0.3).toFixed(2);

    axios
      .post('https://us-central1-academy-val.cloudfunctions.net/api/pay', {
        //.post('/api/pay', {
        idUser,
        id: arq.id,
        nome: arq.nome,
        valor: valor
      })
      .then(data => {
        dispatch(setPayPending(false));
        console.log(data.data.url);
        window.location = data.data.url;
      });
  };
}

export function buscarArquivo(id) {
  return dispatch => {
    dispatch(setFileLoadPending(true));
    dispatch(setFileLoadSuccess(false));
    dispatch(setFileLoadError(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('arquivos')
      .doc(id)
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log('Ok');
        } else {
          let arquivo = doc.data();
          dispatch(setFileLoadData(arquivo));
          dispatch(setFileLoadSuccess(true));
          dispatch(setFileLoadPending(false));
        }
      });
  };
}

export function updateArquivo(arquivo) {
  return dispatch => {
    dispatch(setFileSavePending(true));
    dispatch(setFileSaveSuccess(false));
    dispatch(setFileSaveError(false));

    const idUser = localStorage.getItem('@Academy:user');
    db.collection('academy')
      .doc(idUser)
      .collection('arquivos')
      .doc(arquivo.id)
      .update({
        outputLanguage: arquivo.outputLanguage
      })
      .then(() => {
        dispatch(setFileSavePending(false));
        dispatch(setFileSaveSuccess(true));
      })
      .catch(error => {
        dispatch(setFileSavePending(false));
        dispatch(setFileSaveError(true));
      });
  };
}

export function createDocx(id) {
  return dispatch => {
    const idUser = localStorage.getItem('@Academy:user');

    db.collection('academy')
      .doc(idUser)
      .collection('documentos')
      .doc(id)
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log('Ok');
        } else {
          let documento = doc.data();

          const file = new Document();

          const paragraph = new Paragraph({
            text: documento.texto
          });

          file.addSection({
            children: [paragraph]
          });

          Packer.toBlob(file).then(blob => {
            // saveAs from FileSaver will download the file
            saveAs(blob, documento.titulo + '.docx');
          });
        }
      });
  };
}
