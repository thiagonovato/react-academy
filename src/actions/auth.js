import { auth, provider } from '../configs/Firebase'; // FIXME: RECOMENDO ENCAPSULAR DENTRO DE UM SERVIÇO (Ex: apiService ou coisa do tipo...)
import {
  SET_LOGIN_PENDING,
  SET_LOGIN_SUCCESS,
  SET_LOGIN_ERROR,
  SET_SENHA_PENDING,
  SET_SENHA_SUCCESS,
  SET_SENHA_ERROR,
  SET_CAD_PENDING,
  SET_CAD_SUCCESS,
  SET_CAD_ERROR
} from './types';

function setLoginPending(isLoginPending) {
  return { type: SET_LOGIN_PENDING, isLoginPending };
}

function setLoginSuccess(isLoginSuccess) {
  return { type: SET_LOGIN_SUCCESS, isLoginSuccess };
}

function setLoginError(loginError) {
  return { type: SET_LOGIN_ERROR, loginError };
}

function setSenhaPending(isSenhaPending) {
  return { type: SET_SENHA_PENDING, isSenhaPending };
}

function setSenhaSuccess(isSenhaSuccess) {
  return { type: SET_SENHA_SUCCESS, isSenhaSuccess };
}

function setSenhaError(senhaError) {
  return { type: SET_SENHA_ERROR, senhaError };
}

function setCadPending(isCadPending) {
  return { type: SET_CAD_PENDING, isCadPending };
}

function setCadSuccess(isCadSuccess) {
  return { type: SET_CAD_SUCCESS, isCadSuccess };
}

function setCadError(cadError) {
  return { type: SET_CAD_ERROR, cadError };
}

// Actions login
export function login(email, password) {
  return dispatch => {
    dispatch(setLoginPending(true)); // Estou logando
    dispatch(setLoginSuccess(false)); // Logado = false
    dispatch(setLoginError(null)); // Erro nenhum
    auth
      .signInWithEmailAndPassword(email, password)
      .then(user => {
        localStorage.setItem('@Academy:user', user.user.uid);
        localStorage.setItem('@Academy:email', user.user.email);
        dispatch(setLoginPending(false)); // Não está logando mais
        dispatch(setLoginSuccess(true)); // Logado com sucesso
      })
      .catch(function(err) {
        dispatch(setLoginPending(false)); // Não está logando mais
        dispatch(setLoginError(err)); // Segue o erro
      });
  };
}

export function logout() {
  return dispatch => {
    dispatch(setLoginSuccess(false));
    localStorage.removeItem('@Academy:user');
    localStorage.removeItem('@Academy:email');
    auth.signOut();
    window.location = '/login';
  };
}

export function loginGoogle() {
  return dispatch => {
    dispatch(setLoginSuccess(false));
    dispatch(setLoginError(null));
    auth
      .signInWithPopup(provider)
      .then(function(result) {
        localStorage.setItem('@Academy:user', result.user.uid);
        localStorage.setItem('@Academy:email', result.user.email);
        dispatch(setLoginSuccess(true));
      })
      .catch(function(err) {
        dispatch(setLoginError(err));
      });
  };
}

// Actions Recuperar Senha
export function recuperarSenha(email) {
  return dispatch => {
    dispatch(setSenhaPending(true)); // Estou recuperando senha
    dispatch(setSenhaSuccess(false)); // Logado = false
    dispatch(setSenhaError(null)); // Erro nenhum
    auth
      .sendPasswordResetEmail(email)
      .then(() => {
        dispatch(setSenhaPending(false));
        dispatch(setSenhaSuccess(true));
      })
      .catch(function(err) {
        dispatch(setSenhaPending(false));
        dispatch(setSenhaError(err));
      });
  };
}

export function cadastro(email, password) {
  return dispatch => {
    dispatch(setCadPending(true));
    dispatch(setCadSuccess(false));
    dispatch(setCadError(null));
    auth
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        dispatch(setCadPending(false));
        dispatch(setCadSuccess(true));
      })
      .catch(function(err) {
        dispatch(setCadPending(false));
        dispatch(setCadError(err));
      });
  };
}

export function checarAutenticacao() {
  return dispatch => {
    let usuarioLogado = localStorage.getItem('@Academy:user');
    if (usuarioLogado === null) {
      dispatch(setLoginSuccess(false));
      localStorage.removeItem('@Academy:user');
      auth.signOut();
    } else {
      dispatch(setLoginSuccess(true));
    }
  };
}
