import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import reducers from '../reducers';

export let store;

(function () {
    if (process.env.NODE_ENV === 'production') {
        store = createStore(reducers, applyMiddleware(thunk, logger));
    } else {
        // Redux DevTools
        const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        store = createStore(reducers, composeEnhancers(applyMiddleware(thunk, logger)));
    }
    return store;
})();
