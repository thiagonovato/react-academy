// Recomendo mover este arquivo para a pasta de configs e executa-lo a partir de um apiService
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/auth';
import 'firebase/functions';
import axios from 'axios';

export const api = axios.create({
  baseURL: 'https://academy-val.firebaseio.com/'
});

const config = {
  /*
  apiKey: "AIzaSyD_Qfp0LwwtKPZ_IueiiIUx4ABuhMou2XI",
  authDomain: "academy-react.firebaseapp.com",
  databaseURL: "https://academy-react.firebaseio.com",
  projectId: "academy-react",
  storageBucket: "academy-react.appspot.com",
  messagingSenderId: "658167487098"
*/
  apiKey: 'AIzaSyAhgs_GEEZfXG2s9hUgVbk2yMJrmPobwLk',
  authDomain: 'academy-val.firebaseapp.com',
  databaseURL: 'https://academy-val.firebaseio.com',
  projectId: 'academy-val',
  storageBucket: 'academy-val.appspot.com',
  messagingSenderId: '927333277703'
};
firebase.initializeApp(config);

// Functions
export const functions = firebase.functions();

// Banco de dados Firebase
export const database = firebase.database();
export const db = firebase.firestore();
const settings = { /* your settings... */ timestampsInSnapshots: true };
db.settings(settings);

// Storage
export const storage = firebase.storage();

// Autenticação
export const auth = firebase.auth();
export const provider = new firebase.auth.GoogleAuthProvider();
