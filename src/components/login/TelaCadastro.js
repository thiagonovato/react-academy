import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../actions';
import logo from '../../assets/img/logus-academy-200.png';

class TelaCadastro extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
    this.loginGoogle = this.loginGoogle.bind(this);
    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeInputHandler(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  }

  onSubmit() {
    const { email, password } = this.state;
    this.props.cadastro(email, password);
    this.setState((state, props) => ({ ...state, email: '', password: '' }));
  }

  loginGoogle() {
    this.props.loginGoogle();
  }

  render() {
    let { isCadPending, isCadSuccess, cadError } = this.props;
    const errorMessages = {
      'auth/email-already-in-use': 'Email já existe.',
      'auth/invalid-email': 'E-mail inválido.',
      'auth/weak-password': 'Senha muito fraca.'
    };
    return isCadSuccess ? (
      <Redirect to="/app" />
    ) : (
      <div>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6">
              <div className="row flexbox-center">
                <a href="/">
                  <img src={logo} alt="Academy" />
                </a>
              </div>
              <br />
              <div className="row flexbox-center">
                <h4>Criar novo cadastro</h4>
              </div>
              <br />
              <input
                name="email"
                onChange={this.onChangeInputHandler}
                value={this.state.email}
                type="email"
                placeholder="Email"
              />
              <input
                name="password"
                onChange={this.onChangeInputHandler}
                value={this.state.password}
                type="password"
                placeholder="Password"
              />
              <div className="">
                {isCadPending && <span>Aguarde...</span>}
                {cadError && <span>{errorMessages[cadError.code]}</span>}
                {isCadSuccess && <span>Cadastro efetuado com sucesso!</span>}
              </div>
              <button
                disabled={isCadPending}
                onClick={this.onSubmit}
                type="submit"
              >
                Cadastrar
              </button>
              <br />
              <Link to="/login">Voltar para a tela de Login</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isCadPending: state.AuthStore.isCadPending,
  isCadSuccess: state.AuthStore.isCadSuccess,
  cadError: state.AuthStore.cadError
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TelaCadastro);
