import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../actions';
import logo from '../../assets/img/logus-academy-200.png';

class TelaRecuperarSenha extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '' };
    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeInputHandler(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  }

  onSubmit() {
    const { email } = this.state;
    this.props.recuperarSenha(email);
    this.setState((state, props) => ({ ...state, email: '' }));
  }

  render() {
    const { isSenhaPending, isSenhaSuccess, senhaError } = this.props;
    const errorMessages = {
      'auth/argument-error': 'Campo email vazio',
      'auth/wrong-password': 'Senha incorreta.',
      'auth/invalid-email': 'E-mail inválido.',
      'auth/user-not-found': 'E-mail não cadastrado.',
      'auth/too-many-requests':
        'Várias tentativas sem sucesso. Aguarde um pouco.',
      'auth/cancelled-popup-request':
        'Esta operação foi cancelada devido à abertura de outro popup conflitante. Feche todas as janelas de navegador e tente novamente.'
    };
    return (
      <div>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6">
              <div className="row flexbox-center">
                <a href="/">
                  <img src={logo} alt="Academy" />
                </a>
              </div>
              <br />
              <div className="row flexbox-center">
                <h4>Recuperar senha</h4>
              </div>
              <br />
              <input
                name="email"
                onChange={this.onChangeInputHandler}
                value={this.state.email}
                type="email"
                placeholder="Email"
              />
              <div className="">
                {isSenhaPending && <span>Aguarde...</span>}
                {senhaError && <span>{errorMessages[senhaError.code]}</span>}
                {isSenhaSuccess && (
                  <span>
                    Enviamos um email contendo orientações para recuperar sua
                    senha. Por favor, cheque sua caixa de emails.
                  </span>
                )}
              </div>
              <button
                disabled={isSenhaPending}
                onClick={this.onSubmit}
                type="submit"
              >
                Enviar nova senha
              </button>
              <br />
              <Link to="/login">Voltar para a tela de Login</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isSenhaPending: state.AuthStore.isSenhaPending,
  isSenhaSuccess: state.AuthStore.isSenhaSuccess,
  senhaError: state.AuthStore.senhaError
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TelaRecuperarSenha);
