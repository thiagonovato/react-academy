import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../actions';
import logo from '../../assets/img/logus-academy-200.png';

class TelaLogin extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
    this.loginGoogle = this.loginGoogle.bind(this);
    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.props.checarAutenticacao();
  }

  onChangeInputHandler(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  }

  onSubmit() {
    const { email, password } = this.state;
    this.props.login(email, password);
    this.setState((state, props) => ({ ...state, email: '', password: '' }));
  }

  loginGoogle() {
    this.props.loginGoogle();
  }

  componentDidMount() {
    this.props.checarAutenticacao();
  }

  componentWillReceiveProps() {
    this.props.checarAutenticacao();
  }

  render() {
    let { isLoginPending, isLoginSuccess, loginError } = this.props;
    const errorMessages = {
      'auth/network-request-failed':
        'Erro de rede. Possivelmente você está sem acesso à internet.',
      'auth/email-already-in-use': 'Email já existe.',
      'auth/user-not-found': 'Usuário inexistente.',
      'auth/invalid-email': 'E-mail inválido.',
      'auth/weak-password': 'Senha muito fraca. Tente novamente',
      'auth/wrong-password': 'Email/senha incorreto.',
      'auth/too-many-requests':
        'Muitas tentativas sem sucesso. Aguarde alguns instantes e tente novamente.'
    };

    return isLoginSuccess ? (
      <Redirect to="/app" />
    ) : (
      <div>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6">
              <div className="row flexbox-center">
                <a href="/">
                  <img src={logo} alt="Academy" />
                </a>
              </div>
              <br />
              <div className="row flexbox-center">
                <h4>Acessar</h4>
              </div>
              <br />
              <input
                name="email"
                onChange={this.onChangeInputHandler}
                value={this.state.email}
                type="email"
                placeholder="Email"
              />
              <input
                name="password"
                onChange={this.onChangeInputHandler}
                value={this.state.password}
                type="password"
                placeholder="Password"
              />
              <div className="">
                {isLoginPending && <span>Aguarde...</span>}
                {loginError && <span>{errorMessages[loginError.code]}</span>}
                {isLoginSuccess && <span>Logado!</span>}
              </div>
              <button
                disabled={isLoginPending}
                onClick={this.onSubmit}
                type="submit"
              >
                Acessar
              </button>
              <button
                onClick={this.loginGoogle}
                className="btn btn-block btn-social btn-google btn-flat"
              >
                <i className="fa fa-google-plus" /> Entre usando sua conta
                Google
              </button>
              <Link to="/cadastro">Criar conta</Link>
              <br />
              <Link to="/recuperarSenha">Esqueceu sua senha?</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoginPending: state.AuthStore.isLoginPending,
  isLoginSuccess: state.AuthStore.isLoginSuccess,
  loginError: state.AuthStore.loginError
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TelaLogin);

// const mapDispatchToProps = (dispatch) => {
//     return {
//         login: (email, password) => dispatch(login(email, password)),
//         loginGoogle: () => dispatch(loginGoogle())
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(TelaLogin);
