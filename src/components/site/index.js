import React, { Component } from 'react';

class Index extends Component {
  render() {
    return (
      <div>
        <div id="preloader" />
        <header className="header">
          <div className="container">
            <div className="row flexbox-center">
              <div className="col-lg-2 col-md-3 col-6">
                <div className="logo">
                  <a href="#home">
                    <img src="assets/img/logo.png" alt="logo" />
                  </a>
                </div>
              </div>
              <div className="col-lg-10 col-md-9 col-6">
                <div className="responsive-menu" />
                <div className="mainmenu">
                  <ul id="primary-menu">
                    <li>
                      <a className="nav-link active" href="#home">
                        Home
                      </a>
                    </li>
                    <li>
                      <a className="nav-link" href="#feature">
                        Sobre
                      </a>
                    </li>
                    <li>
                      <a className="nav-link" href="#pricing">
                        Preço
                      </a>
                    </li>
                    <li>
                      <a className="nav-link" href="#contact">
                        Contato
                      </a>
                    </li>
                    <li>
                      <a className="appao-btn" href="/login">
                        Acessar
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </header>
        <section className="hero-area" id="home">
          <div
            className="player"
            data-property="{videoURL:'http://youtu.be/kn-1D5z3-Cs',containment:'#home',autoPlay:true, mute:true, startAt:0, showControls:false, loop:true, opacity:1}"
          />
          <div className="container">
            <div className="row">
              <div className="col-lg-7">
                <div className="hero-area-content">
                  <h1>Software para transcrição áudio</h1>
                  <p>
                    É simples. Envie seu áudio, faça o pagamento (integrado ao
                    PagSeguro) e aguarde o software processar seu arquivo. Você
                    é informado quando finalizar.
                  </p>
                  <a href="/login" className="appao-btn">
                    Acesse o Logus Academy
                  </a>
                  {/* 
                    <a href="/" className="appao-btn">
                    App Store
                    </a>
                  */}
                </div>
              </div>
              {/* 
                <div className="col-lg-5">
                <div className="hand-mockup text-lg-left text-center">
                <img src="assets/img/hand-mockup.png" alt="Hand Mockup" />
                </div>
                </div>
              */}
            </div>
          </div>
        </section>
        <section className="about-area ptb-90">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="sec-title">
                  <h2>
                    Sobre o Logus Academy
                    <span className="sec-title-border">
                      <span />
                      <span />
                      <span />
                    </span>
                  </h2>
                  <p>Quais são as qualidades do Logus Academy?</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="single-about-box">
                  <i className="icofont icofont-ruler-pencil" />
                  <h4>Design Responsivo</h4>
                  <p>
                    O aplicativo se adapta à qualquer dispositivo, seja celular,
                    tablet ou computador/notebook.
                  </p>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="single-about-box active">
                  <i className="icofont icofont-computer" />
                  <h4>Ótima Performance</h4>
                  <p>
                    Utilizamos Inteligencia Artificial para analisar seu áudio e
                    transcrever da forma mais eficiente possível.
                  </p>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="single-about-box">
                  <i className="icofont icofont-headphone-alt" />
                  <h4>Suporte Técnico</h4>
                  <p>
                    Nosso suporte está à disposição para que você tenha a melhor
                    experiência. Basta nos chamar através do Chat clicando no
                    botão do canto inferior direito do site.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="feature-area ptb-90" id="feature">
          <div className="container">
            <div className="row flexbox-center">
              <div className="col-lg-4">
                <div className="single-feature-box text-lg-right text-center">
                  <ul>
                    <li>
                      <div className="feature-box-info">
                        <h4>Melhorias Constantes</h4>
                        <p>
                          Estamos sempre trabalhando para trazer as melhores
                          funcionalidades.
                        </p>
                      </div>
                      <div className="feature-box-icon">
                        <i className="icofont icofont-brush" />
                      </div>
                    </li>
                    <li>
                      <div className="feature-box-info">
                        <h4>Categorias</h4>
                        <p>
                          Categorize todos os seus textos, seja os transcritos
                          ou os que você escrever.
                        </p>
                      </div>
                      <div className="feature-box-icon">
                        <i className="icofont icofont-law-document" />
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="single-feature-box text-lg-left text-center">
                  <ul>
                    <li>
                      <div className="feature-box-icon">
                        <i className="icofont icofont-sun-alt" />
                      </div>
                      <div className="feature-box-info">
                        <h4>Assertividade</h4>
                        <p>
                          Alto índice de assertividade. Consulte nosso FAQ para
                          ver mais detalhes.
                        </p>
                      </div>
                    </li>
                    <li>
                      <div className="feature-box-icon">
                        <i className="icofont icofont-headphone-alt" />
                      </div>
                      <div className="feature-box-info">
                        <h4>Suporte Técnico</h4>
                        <p>
                          Estamos sempre disponível para lhe ajudar. Fale
                          conosco pelo chat.
                        </p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="showcase-area ptb-90">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="sec-title">
                  <h2>
                    O que você precisa saber?
                    <span className="sec-title-border">
                      <span />
                      <span />
                      <span />
                    </span>
                  </h2>
                  <p>
                    Veja abaixo os principais pontos a serem levados em
                    consideração para ter uma ótima experiência.
                  </p>
                </div>
              </div>
            </div>
            <div className="row flexbox-center">
              <div className="col-lg-6">
                <div className="single-showcase-box">
                  <h4>Inteligência Artificial</h4>
                  <p>
                    Utilizamos um sistema de Inteligência Artificial que
                    interpreta seu arquivo de áudio. E o Logus Academy já coloca
                    a pontuação no seu texto de forma automática.
                  </p>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="single-showcase-box">
                  <h4>Qualidade do áudio</h4>
                  <p>
                    Seu áudio deve ter boa qualidade para que sua transcrição
                    tenha um alto índice de assertividade. Ruidos e/ou sons que
                    não são dos interlocutores podem prejudicar o entendimento
                    do Logus Academy, reduzindo a qualidade da transcrição.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="pricing-area ptb-90" id="pricing">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="sec-title">
                  <h2>
                    Quanto custa?
                    <span className="sec-title-border">
                      <span />
                      <span />
                      <span />
                    </span>
                  </h2>
                  <p>
                    Atualmente cobramos um valor fixo por minuto transcrito.
                  </p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4" />
              <div className="col-lg-4">
                <div className="single-pricing-box">
                  <div className="price">
                    <h1>
                      <span>R$</span>
                      0,30
                    </h1>
                    <p>Trinta centavos/minuto</p>
                  </div>
                  <div className="price-details">
                    <ul>
                      <li>Envie seu áudio</li>
                      <li>Faça o pagamento (através do PagSeguro)</li>
                      <li>Aguarde a transcrição finalizar</li>
                    </ul>
                    <a className="appao-btn" href="/login">
                      Acessar o Logus Academy
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4" />
            </div>
          </div>
        </section>

        <footer className="footer" id="contact" style={{ marginTop: '150px' }}>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-6">
                <div className="contact-address">
                  <h4>Contato</h4>
                  <p>
                    Estamos inteiramente disponível em algum dos canais abaixo.
                  </p>
                  <ul>
                    <li>
                      <div className="contact-address-icon">
                        <i className="icofont icofont-brand-whatsapp" />
                      </div>
                      <div className="contact-address-info">
                        <a
                          target="_blank"
                          rel="noopener noreferrer"
                          href="https://api.whatsapp.com/send?phone=5562992889776&text=Ol%C3%A1!%20Estou%20entrando%20em%20contato%20atrav%C3%A9s%20do%20site%20Logus%20Academy."
                        >
                          +55 62 99288-9776
                        </a>
                      </div>
                    </li>
                    <li>
                      <div className="contact-address-icon">
                        <i className="icofont icofont-envelope" />
                      </div>
                      <div className="contact-address-info">
                        <a href="mailto:contato@logusacademy.com.br">
                          contato@logusacademy.com.br
                        </a>
                      </div>
                    </li>
                    <li>
                      <div className="contact-address-icon">
                        <i className="icofont icofont-chat" />
                      </div>
                      <div className="contact-address-info">
                        Clique no botão de chat abaixo
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div className="copyright-area">
                  <ul>
                    <li>
                      <a
                        href="https://www.instagram.com/logusacademy/"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i className="icofont icofont-social-instagram" />
                      </a>
                    </li>
                  </ul>
                  <p>
                    &copy; Copyright &copy; All rights reserved | This template
                    is made with{' '}
                    <i className="fa fa-heart-o" aria-hidden="true" /> by{' '}
                    <a
                      href="https://colorlib.com"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Colorlib
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </footer>
        <a href="/" className="scrollToTop">
          <i className="icofont icofont-arrow-up" />
        </a>
      </div>
    );
  }
}
export default Index;
