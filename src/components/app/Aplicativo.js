import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../actions';

// Impots de componentes
import Menubar from './Menubar';

import Categorias from './categorias/Categorias';
import Categoriasnova from './categorias/CategoriasNova';
import Categoriasedita from './categorias/Categoriasedita';

import Documentos from './documentos/Documentos';
import DocumentosNovo from './documentos/DocumentosNovo';
import Documentosedita from './documentos/Documentosedita';

import Upload from './upload/Upload';
import UploadNovo from './upload/UploadNovo';
import Uploadedita from './upload/Uploadedita';

class Aplicativo extends Component {
  constructor(props) {
    super(props);

    this.props.checarAutenticacao();
  }

  componentDidMount() {
    this.props.checarAutenticacao();
  }

  componentWillReceiveProps() {
    this.props.checarAutenticacao();
  }

  render() {
    let { isLoginSuccess } = this.props;
    if (!isLoginSuccess) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <Menubar />
        <Route exact path="/app/upload" component={Upload} />
        <Route exact path="/app/upload-novo" component={UploadNovo} />
        <Route exact path="/app/upload/edita/:id" component={Uploadedita} />

        <Route exact path="/app/documentos" component={Documentos} />
        <Route exact path="/app/documentos-novo" component={DocumentosNovo} />
        <Route path={'/app/documentos/edita/:id'} component={Documentosedita} />

        <Route exact path="/app/categorias" component={Categorias} />
        <Route exact path="/app/categorias-nova" component={Categoriasnova} />
        <Route path={'/app/categorias/edita/:id'} component={Categoriasedita} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoginSuccess: state.AuthStore.isLoginSuccess
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Aplicativo);
