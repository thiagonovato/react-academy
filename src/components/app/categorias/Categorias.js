import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link } from 'react-router-dom';

import { FaPen, FaTrashAlt } from 'react-icons/fa';

class Categorias extends Component {
  constructor(props) {
    super(props);

    this.deleteCategoria = this.deleteCategoria.bind(this);
  }

  componentDidMount() {
    this.props.listarCategorias();
  }

  deleteCategoria(id) {
    this.props.deleteCategoria(id);
  }

  render() {
    let {
      isCategoriaPending,
      isCategoriaErro,
      isCategoriaSuccess,
      categoriaData
    } = this.props;
    return (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>Categorias</h3>
        <div className="row">
          <Link to="/app/categorias-nova" className="btn btn-primary">
            Nova categoria
          </Link>
        </div>
        <div className="row">
          {isCategoriaPending && <span>Aguarde...</span>}
          {isCategoriaErro && <span>Nenhuma categoria cadastrada.</span>}
          {isCategoriaSuccess && (
            <table className="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {categoriaData.map(
                  function(cat) {
                    return (
                      <tr key={cat.id}>
                        <td>{cat.nome}</td>
                        <td>
                          <Link
                            to={'/app/categorias/edita/' + cat.id}
                            className="btn btn-primary"
                          >
                            <FaPen />
                          </Link>
                          <button
                            onClick={() =>
                              window.confirm(
                                `Confirma a exclusão da categoria ${cat.nome}?`
                              ) && this.deleteCategoria(cat.id)
                            }
                            disabled={isCategoriaPending}
                            className="btn btn-danger"
                          >
                            <FaTrashAlt />
                          </button>
                        </td>
                      </tr>
                    );
                  }.bind(this)
                )}
              </tbody>
            </table>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isCategoriaPending: state.categorias.isCategoriaPending,
  isCategoriaErro: state.categorias.isCategoriaErro,
  isCategoriaSuccess: state.categorias.isCategoriaSuccess,
  categoriaData: state.categorias.categoriaData
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categorias);
