import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link, Redirect } from 'react-router-dom';

class Categoriaedita extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: ''
    };

    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps() {
    this.setState({
      nome: this.props.categoriaSearchData.nome
    });
  }

  componentDidMount() {
    this.props.buscarCategoria(this.props.match.params.id);
  }

  onChangeInputHandler(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  }

  onSubmit(e) {
    const { nome } = this.state;
    let id = this.props.match.params.id;
    this.props.updateCategoria(id, nome);
    this.setState((state, props) => ({ ...state, nome: '' }));
  }

  render() {
    let {
      isSearchCategoriaPending,
      isSearchCategoriaErro,
      isSearchCategoriaSuccess,
      isUpdateCategoriaPending,
      isUpdateCategoriaErro,
      isUpdateCategoriaSuccess
    } = this.props;

    return isUpdateCategoriaSuccess ? (
      <Redirect to="/app/categorias" />
    ) : (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>
          Edita categoria
        </h3>
        <div className="row">
          <Link to="/app/categorias" className="btn btn-primary">
            Voltar
          </Link>
        </div>
        <div className="row">
          <br />
          {isSearchCategoriaPending && <span>Aguarde...</span>}
          {isSearchCategoriaErro && <span>Erro ao buscar.</span>}
        </div>
        {isSearchCategoriaSuccess && (
          <div>
            <div className="row">
              <input
                value={this.state.nome}
                onChange={this.onChangeInputHandler}
                className="form-control"
                name="nome"
                type="text"
                placeholder="Digite o nome da categoria"
              />
            </div>
            <div className="row">
              <button
                onClick={this.onSubmit}
                disabled={isUpdateCategoriaPending}
                className="btn btn-primary"
                type="submit"
              >
                Salvar
              </button>
              <br />
              {isUpdateCategoriaPending && <span>Aguarde...</span>}
              {isUpdateCategoriaErro && <span>Erro ao salvar.</span>}
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isSearchCategoriaPending: state.categorias.isSearchCategoriaPending,
  isSearchCategoriaErro: state.categorias.isSearchCategoriaErro,
  isSearchCategoriaSuccess: state.categorias.isSearchCategoriaSuccess,
  categoriaSearchData: state.categorias.categoriaSearchData,
  isUpdateCategoriaPending: state.categorias.isUpdateCategoriaPending,
  isUpdateCategoriaErro: state.categorias.isUpdateCategoriaErro,
  isUpdateCategoriaSuccess: state.categorias.isUpdateCategoriaSuccess
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categoriaedita);
