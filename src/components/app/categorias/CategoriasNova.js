import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link, Redirect } from 'react-router-dom';

class CategoriaNova extends Component {
  constructor(props) {
    super(props);

    this.state = { nome: '' };
    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeInputHandler(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  }

  onSubmit() {
    const { nome } = this.state;
    this.props.createCategoria(nome);
    this.setState((state, props) => ({ ...state, nome: '' }));
  }

  render() {
    let {
      isCreateCategoriaErro,
      isCreateCategoriaPending,
      isCreateCategoriaSuccess
    } = this.props;
    return isCreateCategoriaSuccess ? (
      <Redirect to="/app/categorias" />
    ) : (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>
          Nova categoria
        </h3>
        <div className="row">
          <Link to="/app/categorias" className="btn btn-primary">
            Voltar
          </Link>
        </div>
        <div className="row">
          <br />
          {isCreateCategoriaPending && <span>Aguarde...</span>}
          {isCreateCategoriaErro && <span>Erro.</span>}
          {isCreateCategoriaSuccess && (
            <span>Categoria salva com sucesso.</span>
          )}
        </div>
        <div className="row">
          <input
            onChange={this.onChangeInputHandler}
            className="form-control"
            value={this.state.nome}
            name="nome"
            type="text"
            placeholder="Digite o nome da categoria"
          />
        </div>
        <div className="row">
          <button
            onClick={this.onSubmit}
            disabled={isCreateCategoriaPending}
            className="btn btn-primary"
            type="submit"
          >
            Salvar
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isCreateCategoriaPending: state.categorias.isCreateCategoriaPending,
  isCreateCategoriaErro: state.categorias.isCreateCategoriaErro,
  isCreateCategoriaSuccess: state.categorias.isCreateCategoriaSuccess
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoriaNova);
