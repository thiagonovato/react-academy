import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link } from 'react-router-dom';

import { FaPen, FaTrashAlt } from 'react-icons/fa';

class Documentos extends Component {
  constructor(props) {
    super(props);

    this.deleteDocumento = this.deleteDocumento.bind(this);
  }

  componentDidMount() {
    this.props.listarDocumentos();
  }

  deleteDocumento(id) {
    this.props.deleteDocumento(id);
  }

  render() {
    let {
      isDocumentoPending,
      isDocumentoErro,
      isDocumentoSuccess,
      documentoData
    } = this.props;
    return (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>Documentos</h3>
        <div className="row">
          <Link to="/app/documentos-novo" className="btn btn-primary">
            Novo documento
          </Link>
        </div>
        <div className="row">
          {isDocumentoPending && <span>Aguarde...</span>}
          {isDocumentoErro && <span>Nenhum documento cadastrado.</span>}
          {isDocumentoSuccess && (
            <table className="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>Título</th>
                  <th>Categoria</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {documentoData.map(
                  function(doc) {
                    return (
                      <tr key={doc.id}>
                        <td>{doc.titulo}</td>
                        <td>{doc.categoria}</td>
                        <td>
                          <Link
                            to={'/app/documentos/edita/' + doc.id}
                            className="btn btn-primary"
                          >
                            <FaPen />
                          </Link>
                          <button
                            onClick={() =>
                              window.confirm(
                                `Confirma a exclusão do documento ${
                                  doc.titulo
                                }?`
                              ) && this.deleteDocumento(doc.id)
                            }
                            disabled={isDocumentoPending}
                            className="btn btn-danger"
                          >
                            <FaTrashAlt />
                          </button>
                        </td>
                      </tr>
                    );
                  }.bind(this)
                )}
              </tbody>
            </table>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isDocumentoPending: state.documentos.isDocumentoPending,
  isDocumentoErro: state.documentos.isDocumentoErro,
  isDocumentoSuccess: state.documentos.isDocumentoSuccess,
  documentoData: state.documentos.documentoData
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Documentos);
