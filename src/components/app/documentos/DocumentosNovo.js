import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link, Redirect } from 'react-router-dom';

class DocumentoNova extends Component {
  constructor(props) {
    super(props);

    this.state = {
      titulo: '',
      texto: '',
      categoria: ''
    };

    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.props.listarCategorias();
  }

  onChangeInputHandler(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  }

  onSubmit() {
    const { titulo } = this.state;
    const { texto } = this.state;
    const { categoria } = this.state;
    const documento = {
      titulo: titulo,
      texto: texto,
      categoria: categoria
    };

    this.props.createDocumento(documento);
    this.setState((state, props) => ({
      ...state,
      titulo: '',
      texto: '',
      categoria: ''
    }));
  }

  render() {
    let {
      isCreateDocumentoErro,
      isCreateDocumentoPending,
      isCreateDocumentoSuccess,
      isCategoriaPending,
      isCategoriaSuccess,
      categoriaData
    } = this.props;
    return isCreateDocumentoSuccess ? (
      <Redirect to="/app/documentos" />
    ) : (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>
          Novo documento
        </h3>
        <div className="row">
          <Link to="/app/documentos" className="btn btn-primary">
            Voltar
          </Link>
        </div>
        <div className="row">
          <br />
          {isCreateDocumentoPending && <span>Aguarde...</span>}
          {isCreateDocumentoErro && <span>Erro.</span>}
          {isCreateDocumentoSuccess && (
            <span>Documento salvo com sucesso.</span>
          )}
        </div>
        <div className="row">
          <input
            onChange={this.onChangeInputHandler}
            className="form-control"
            value={this.state.titulo}
            name="titulo"
            type="text"
            placeholder="Digite o título do documento"
          />
        </div>
        <br />
        <div className="row">
          <textarea
            rows="10"
            onChange={this.onChangeInputHandler}
            className="form-control"
            value={this.state.texto}
            name="texto"
            type="text"
          />
        </div>
        <div className="row">
          {isCategoriaPending && (
            <span>Aguarde... carregando categorias...</span>
          )}
          {isCategoriaSuccess && (
            <select
              onChange={this.onChangeInputHandler}
              className="form-control"
              value={this.state.categoria}
              name="categoria"
            >
              <option>-- Selecione --</option>
              {categoriaData.map(function(cat) {
                return (
                  <option key={cat.id} value={cat.nome}>
                    {cat.nome}
                  </option>
                );
              })}
            </select>
          )}
        </div>
        <div className="row">
          <button
            onClick={this.onSubmit}
            disabled={isCreateDocumentoPending}
            className="btn btn-primary"
            type="submit"
          >
            Salvar
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isCreateDocumentoPending: state.documentos.isCreateDocumentoPending,
  isCreateDocumentoErro: state.documentos.isCreateDocumentoErro,
  isCreateDocumentoSuccess: state.documentos.isCreateDocumentoSuccess,

  isCategoriaPending: state.categorias.isCategoriaPending,
  isCategoriaErro: state.categorias.isCategoriaErro,
  isCategoriaSuccess: state.categorias.isCategoriaSuccess,
  categoriaData: state.categorias.categoriaData
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentoNova);
