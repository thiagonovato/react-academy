import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link, Redirect } from 'react-router-dom';

class Documentoedita extends Component {
  constructor(props) {
    super(props);

    this.state = {
      titulo: '',
      texto: '',
      categoria: ''
    };

    this.onChangeInputHandler = this.onChangeInputHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.props.listarCategorias();
  }

  componentWillReceiveProps() {
    this.setState({
      titulo: this.props.documentoSearchData.titulo,
      texto: this.props.documentoSearchData.texto,
      categoria: this.props.documentoSearchData.categoria
    });
  }

  componentDidMount() {
    this.props.buscarDocumento(this.props.match.params.id);
  }

  onChangeInputHandler(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  }

  onSubmit(e) {
    const { titulo } = this.state;
    const { texto } = this.state;
    const { categoria } = this.state;
    let id = this.props.match.params.id;

    const documento = {
      id: id,
      titulo: titulo,
      texto: texto,
      categoria: categoria
    };
    this.props.updateDocumento(documento);
    this.setState((state, props) => ({
      ...state,
      titulo: '',
      texto: '',
      categoria: ''
    }));
  }

  render() {
    let {
      isSearchDocumentoPending,
      isSearchDocumentoErro,
      isSearchDocumentoSuccess,
      isUpdateDocumentoPending,
      isUpdateDocumentoErro,
      isUpdateDocumentoSuccess,
      isCategoriaPending,
      isCategoriaSuccess,
      categoriaData
    } = this.props;

    return isUpdateDocumentoSuccess ? (
      <Redirect to="/app/documentos" />
    ) : (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>
          Edita documento
        </h3>
        <div className="row">
          <Link to="/app/documentos" className="btn btn-primary">
            Voltar
          </Link>
        </div>
        <div className="row">
          <br />
          {isSearchDocumentoPending && <span>Aguarde...</span>}
          {isSearchDocumentoErro && <span>Erro ao buscar.</span>}
        </div>
        {isSearchDocumentoSuccess && (
          <div>
            <div className="row">
              <input
                value={this.state.titulo}
                onChange={this.onChangeInputHandler}
                className="form-control"
                name="titulo"
                type="text"
                placeholder="Digite o título da documento"
              />
            </div>
            <br />
            <div className="row">
              <textarea
                value={this.state.texto}
                rows="10"
                onChange={this.onChangeInputHandler}
                className="form-control"
                name="texto"
                type="text"
              />
            </div>
            <div className="row">
              {isCategoriaPending && (
                <span>Aguarde... carregando categorias...</span>
              )}
              {isCategoriaSuccess && (
                <select
                  onChange={this.onChangeInputHandler}
                  className="form-control"
                  value={this.state.categoria}
                  name="categoria"
                >
                  <option>-- Selecione --</option>
                  {categoriaData.map(function(cat) {
                    return (
                      <option key={cat.id} value={cat.nome}>
                        {cat.nome}
                      </option>
                    );
                  })}
                </select>
              )}
            </div>
            <div className="row">
              <button
                onClick={this.onSubmit}
                disabled={isUpdateDocumentoPending}
                className="btn btn-primary"
                type="submit"
              >
                Salvar
              </button>
              <br />
              {isUpdateDocumentoPending && <span>Aguarde...</span>}
              {isUpdateDocumentoErro && <span>Erro ao salvar.</span>}
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isSearchDocumentoPending: state.documentos.isSearchDocumentoPending,
  isSearchDocumentoErro: state.documentos.isSearchDocumentoErro,
  isSearchDocumentoSuccess: state.documentos.isSearchDocumentoSuccess,
  documentoSearchData: state.documentos.documentoSearchData,
  isUpdateDocumentoPending: state.documentos.isUpdateDocumentoPending,
  isUpdateDocumentoErro: state.documentos.isUpdateDocumentoErro,
  isUpdateDocumentoSuccess: state.documentos.isUpdateDocumentoSuccess,

  sCategoriaPending: state.categorias.isCategoriaPending,
  isCategoriaErro: state.categorias.isCategoriaErro,
  isCategoriaSuccess: state.categorias.isCategoriaSuccess,
  categoriaData: state.categorias.categoriaData
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Documentoedita);
