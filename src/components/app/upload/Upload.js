import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link } from 'react-router-dom';

import { FaPen, FaTrashAlt, FaDownload } from 'react-icons/fa';

class Upload extends Component {
  constructor(props) {
    super(props);

    this.state = {
      file: '',
      idioma: ''
    };
  }

  componentDidMount() {
    this.props.listarArquivos();
  }

  handleFileSelect = e => {
    this.setState({ file: e.target.files[0] });
  };

  onChangeInputHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  };

  onSubmit = () => {
    this.props.uploadFile(this.state.file);
  };

  deleteArquivo(id) {
    this.props.deleteArquivo(id);
  }

  transcreveArquivoPT(id) {
    this.props.transcreveArquivo(id, 'pt-BR');
  }

  transcreveArquivoEN(id) {
    this.props.transcreveArquivo(id, 'en-US');
  }

  handlePay(arq) {
    this.props.pay(arq);
  }

  renderSwithStatus(status) {
    switch (status) {
      case '0':
        return 'Aguardando pagamento';
      case '1':
        return 'Aguardando pagamento';
      case '2':
        return 'Em análise';
      case '3':
        return 'Pago';
      case '6':
        return 'Devolvido';
      case '7':
        return 'Cancelado';
      default:
        return 'Aguardando pagamento';
    }
  }

  renderSwithLanguage(language) {
    switch (language) {
      case 'pt-BR':
        return 'Português';
      case 'en-US':
        return 'Inglês';
      default:
        return 'Não definido';
    }
  }

  createDocx(arq) {
    this.props.createDocx(arq.linkDocumento);
    /*
    console.log('arquivo', arq);
    const doc = new Document();

    const paragraph = new Paragraph({
      text: arq.texto
    });

    doc.addSection({
      children: [paragraph]
    });

    Packer.toBlob(doc).then(blob => {
      // saveAs from FileSaver will download the file
      saveAs(blob, 'example.docx');
    });
    */
  }

  render() {
    let {
      uploadDataList,
      isUploadDataPending,
      isUploadDataSuccess,
      isUploadDataErro,
      isDeleteArquivoPending,
      isDeleteArquivoError,
      isPayPending
    } = this.props;
    return (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>
          Lista dos arquivos
        </h3>
        <div className="row">
          <Link to="/app/upload-novo" className="btn btn-primary">
            Novo upload
          </Link>
        </div>
        <div>
          {isUploadDataPending && <span>Carregando...</span>}
          {isPayPending && (
            <span>Aguarde... você será redirecionado ao PagSeguro.</span>
          )}
          {isUploadDataErro && <span>Nenhum arquivo localizado.</span>}
          {isDeleteArquivoError && <span>Erro ao deletar o arquivo.</span>}
          {isUploadDataSuccess && (
            <table className="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Tamanho (min)</th>
                  <th>Idioma do texto</th>
                  <th>Status Pagamento</th>
                  <th>Transcrição</th>
                  <th>Preço</th>
                  <th>Comprar</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {uploadDataList.map(
                  function(arq) {
                    return (
                      <tr key={arq.id}>
                        <td>
                          <a
                            href={arq.link}
                            target="_blank"
                            rel="noopener noreferrer"
                            style={{ fontWeight: 'bold' }}
                          >
                            {arq.nome}
                          </a>
                        </td>
                        <td>{(parseInt(arq.comprimento) / 60).toFixed(2)}</td>
                        <td>{this.renderSwithLanguage(arq.outputLanguage)}</td>
                        <td>{this.renderSwithStatus(arq.statusPayment)}</td>
                        <td>
                          {arq.status === 'Transcrito' ? (
                            <Link
                              to={`/app/documentos/edita/${arq.linkDocumento}`}
                              className="btn btn-primary"
                            >
                              Ver Transcrição
                            </Link>
                          ) : arq.status === 'Não transcrito' ? (
                            'Não transcrito'
                          ) : (
                            arq.status + ' - ' + arq.progressPercent + ' %'
                          )}
                        </td>
                        <td>
                          {parseInt(arq.comprimento) / 60 < 1
                            ? 'R$ 1,00'
                            : 'R$ ' +
                              ((parseInt(arq.comprimento) / 60) * 0.3).toFixed(
                                2
                              )}
                        </td>
                        <td>
                          <button
                            onClick={() => this.handlePay(arq)}
                            className="btn btn-success"
                            disabled={isPayPending || arq.statusPayment === '3'}
                          >
                            {arq.statusPayment === '3'
                              ? 'Concluída'
                              : 'Comprar'}
                          </button>
                        </td>
                        <td>
                          {arq.statusPayment !== '3' ? (
                            <Link
                              to={`/app/upload/edita/${arq.id}`}
                              className="btn btn-dark"
                            >
                              <FaPen />
                            </Link>
                          ) : (
                            <button
                              className="btn btn-primary"
                              onClick={() => this.createDocx(arq)}
                            >
                              <FaDownload />
                            </button>
                          )}
                          <button
                            disabled={isDeleteArquivoPending}
                            onClick={() =>
                              window.confirm(
                                `Confirma a exclusão do arquivo ${arq.nome}?`
                              ) && this.deleteArquivo(arq)
                            }
                            className="btn btn-danger"
                          >
                            <FaTrashAlt />
                          </button>
                        </td>
                      </tr>
                    );
                  }.bind(this)
                )}
              </tbody>
            </table>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  uploadDataList: state.upload.uploadDataList,
  isUploadDataPending: state.upload.isUploadDataPending,
  isUploadDataSuccess: state.upload.isUploadDataSuccess,
  isUploadDataErro: state.upload.isUploadDataErro,
  isDeleteArquivoPending: state.upload.isDeleteArquivoPending,
  isDeleteArquivoError: state.upload.isDeleteArquivoError,
  isStatusTranscrevendo: state.upload.isStatusTranscrevendo,
  isPayPending: state.upload.isPayPending
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Upload);
