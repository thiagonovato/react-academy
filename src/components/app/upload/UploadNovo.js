import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';

class UploadNovo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      file: '',
      errorFormatFile: false
    };
  }

  handleFileSelect = e => {
    const type = e.target.files[0].type;
    if (
      type === 'audio/wav' ||
      type === 'audio/flac' ||
      type === 'audio/x-flac' ||
      type === 'audio/mp3' ||
      type === 'audio/m4a' ||
      type === 'audio/x-m4a'
    ) {
      this.setState({ file: e.target.files[0] });
      this.setState({ errorFormatFile: false });
    } else {
      this.setState({ errorFormatFile: true });
    }
  };

  onSubmit = () => {
    this.props.uploadFile(this.state.file);
  };

  render() {
    let {
      isUploadPending,
      uploadPercent,
      isUploadError,
      isUploadSuccess,
      isUploadCreateDocPending,
      isUploadCreateDocSuccess
    } = this.props;

    const { errorFormatFile } = this.state;

    return isUploadCreateDocSuccess ? (
      <Redirect to="/app/upload" />
    ) : (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>
          Upload de arquivo
        </h3>
        <div className="row">
          <Link to="/app/upload" className="btn btn-primary">
            Voltar
          </Link>
        </div>
        <br />
        <div className="row">
          <div className="alert alert-danger" role="alert">
            FormatoS de arquivo permitido: <strong>MP3, M4A, WAV e FLAC</strong>
            . Caso seu arquivo de áudio não esteja neste formato, use o
            aplicativo{' '}
            <a
              rel="noopener noreferrer"
              href="https://www.audacityteam.org/download/"
              target="_blank"
            >
              Audacity
            </a>{' '}
            para fazer a conversão.
          </div>
          <input
            className="form-control"
            type="file"
            onChange={this.handleFileSelect}
          />
          <button
            className="btn btn-primary"
            disabled={isUploadPending || errorFormatFile}
            onClick={this.onSubmit}
          >
            Enviar
          </button>
        </div>
        <div className="row">
          {isUploadPending && (
            <span>{`${parseInt(uploadPercent).toFixed(0)} %`}</span>
          )}
          {errorFormatFile && (
            <span>
              É possível enviar arquivos apenas nos seguintes formatos: mp3,
              m4a, wav e flac.
            </span>
          )}
          {isUploadError && <span>Erro ao enviar o arquivo.</span>}
          {isUploadSuccess &&
            !isUploadCreateDocPending && (
              <span>Arquivo enviado com sucesso.</span>
            )}
          {isUploadCreateDocPending && <span>Aguarde...</span>}
          {isUploadCreateDocSuccess &&
            isUploadCreateDocPending && <span>Envio finalizado.</span>}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isUploadPending: state.upload.isUploadPending,
  uploadPercent: state.upload.uploadPercent,
  isUploadError: state.upload.isUploadError,
  isUploadSuccess: state.upload.isUploadSuccess,
  isUploadCreateDocPending: state.upload.isUploadCreateDocPending,
  isUploadCreateDocSuccess: state.upload.isUploadCreateDocSuccess
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadNovo);
