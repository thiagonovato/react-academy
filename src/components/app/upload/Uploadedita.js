import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../../actions';
import { Link, Redirect } from 'react-router-dom';

class Uploadedita extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      outputLanguage: ''
    };
  }
  componentDidMount() {
    this.props.buscarArquivo(this.props.match.params.id);
  }

  componentWillReceiveProps() {
    this.setState({
      nome: this.props.fileLoadData.nome,
      outputLanguage: this.props.fileLoadData.outputLanguage
    });
  }

  onChangeInputHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState((state, props) => ({ ...state, [name]: value }));
  };

  onSubmit = e => {
    const { outputLanguage } = this.state;
    let id = this.props.match.params.id;

    const arquivo = {
      id: id,
      outputLanguage
    };

    this.props.updateArquivo(arquivo);
  };

  render() {
    let {
      isFileLoadError,
      isFileLoadPending,
      isFileLoadSuccess,
      isFileSavePending,
      isFileSaveSuccess,
      isFileSaveError
    } = this.props;
    return isFileSaveSuccess ? (
      <Redirect to="/app/upload" />
    ) : (
      <div className="container">
        <h3 style={{ marginTop: '10px', marginBottom: '10px' }}>
          Edita arquivo
        </h3>
        <div className="row">
          <Link to="/app/upload" className="btn btn-primary">
            Voltar
          </Link>
        </div>
        <div className="row">
          <br />
          {isFileLoadPending && <span>Aguarde...</span>}
          {isFileLoadError && <span>Erro ao buscar.</span>}
        </div>
        {isFileLoadSuccess && (
          <div>
            <div className="row">
              <label>Idioma da transcrição</label>
              <select
                onChange={this.onChangeInputHandler}
                className="form-control"
                value={this.state.outputLanguage}
                name="outputLanguage"
              >
                <option>-- Selecione --</option>
                <option value="pt-BR">Português</option>
                <option value="en-US">Inglês</option>
              </select>
            </div>
            <br />
            <div className="row">
              <button
                onClick={this.onSubmit}
                className="btn btn-primary"
                type="submit"
              >
                Salvar
              </button>
              <br />
              {isFileSavePending && <span>Aguarde...</span>}
              {isFileSaveError && <span>Erro ao salvar.</span>}
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isFileLoadPending: state.upload.isFileLoadPending,
  isFileLoadSuccess: state.upload.isFileLoadSuccess,
  isFileLoadError: state.upload.isFileLoadError,
  fileLoadData: state.upload.fileLoadData,

  isFileSavePending: state.upload.isFileSavePending,
  isFileSaveSuccess: state.upload.isFileSaveSuccess,
  isFileSaveError: state.upload.isFileSaveError
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Uploadedita);
