import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from '../actions';

import { PrivateRoute } from './PrivateRoute';
import Site from '../components/site/index';
import Auth from '../components/login/TelaLogin';
import Cadastro from '../components/login/TelaCadastro';
import Senha from '../components/login/TelaRecuperarSenha';
import Main from '../components/app/Aplicativo';

class Routes extends Component {
  constructor(props) {
    super(props);

    this.props.checarAutenticacao();
  }

  componentDidMount() {
    this.props.checarAutenticacao();
  }

  componentWillReceiveProps() {
    this.props.checarAutenticacao();
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Site} />
          <Route exact path="/login" component={Auth} />
          <Route exact path="/cadastro" component={Cadastro} />
          <Route exact path="/recuperarSenha" component={Senha} />
          <PrivateRoute path="/app" component={Main} />
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => ({
  isLoginSuccess: state.AuthStore.isLoginSuccess
});
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routes);
