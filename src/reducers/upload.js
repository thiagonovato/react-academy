import {
  UPLOAD_PENDING,
  UPLOAD_PERCENT,
  UPLOAD_ERROR,
  UPLOAD_SUCCESS,
  UPLOAD_CREATE_DOC_PENDING,
  UPLOAD_CREATE_DOC_SUCCESS,
  LOAD_ARQUIVOS_DATA,
  LOAD_ARQUIVOS_DATA_PENDING,
  LOAD_ARQUIVOS_DATA_SUCCESS,
  LOAD_ARQUIVOS_DATA_ERRO,
  DELETE_ARQUIVO_PENDING,
  DELETE_ARQUIVO_ERROR,
  STATUS_TRANSCREVENDO,
  PAY_PENDING,
  FILE_LOAD_PENDING,
  FILE_LOAD_SUCCESS,
  FILE_LOAD_ERROR,
  FILE_LOAD_DATA,
  FILE_SAVE_PENDING,
  FILE_SAVE_SUCCESS,
  FILE_SAVE_ERROR
} from '../actions/types';

const INITIAL_STATE = {
  isUploadPending: false,
  uploadPercent: '',
  isUploadError: false,
  isUploadSuccess: false,
  isUploadCreateDocPending: false,
  isUploadCreateDocSuccess: false,
  uploadDataList: [],
  isUploadDataPending: false,
  isUploadDataSuccess: false,
  isUploadDataErro: false,
  isDeleteArquivoPending: false,
  isStatusTranscrevendo: false,
  isPayPending: false,
  isFileLoadPending: false,
  isFileLoadSuccess: false,
  isFileLoadError: false,
  fileLoadData: [],
  isFileSavePending: false,
  isFileSaveSuccess: false,
  isFileSaveError: false
};

const reducers = function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPLOAD_PENDING:
      return { ...state, isUploadPending: action.isUploadPending };
    case UPLOAD_PERCENT:
      return { ...state, uploadPercent: action.uploadPercent };
    case UPLOAD_ERROR:
      return { ...state, isUploadError: action.isUploadError };
    case UPLOAD_SUCCESS:
      return { ...state, isUploadSuccess: action.isUploadSuccess };
    case UPLOAD_CREATE_DOC_PENDING:
      return {
        ...state,
        isUploadCreateDocPending: action.isUploadCreateDocPending
      };
    case UPLOAD_CREATE_DOC_SUCCESS:
      return {
        ...state,
        isUploadCreateDocSuccess: action.isUploadCreateDocSuccess
      };
    case LOAD_ARQUIVOS_DATA:
      return { ...state, uploadDataList: action.uploadDataList };
    case LOAD_ARQUIVOS_DATA_PENDING:
      return { ...state, isUploadDataPending: action.isUploadDataPending };
    case LOAD_ARQUIVOS_DATA_SUCCESS:
      return { ...state, isUploadDataSuccess: action.isUploadDataSuccess };
    case LOAD_ARQUIVOS_DATA_ERRO:
      return { ...state, isUploadDataErro: action.isUploadDataErro };
    case DELETE_ARQUIVO_PENDING:
      return {
        ...state,
        isDeleteArquivoPending: action.isDeleteArquivoPending
      };
    case DELETE_ARQUIVO_ERROR:
      return { ...state, isDeleteArquivoError: action.isDeleteArquivoError };
    case STATUS_TRANSCREVENDO:
      return { ...state, isStatusTranscrevendo: action.isStatusTranscrevendo };

    // PAY
    case PAY_PENDING:
      return { ...state, isPayPending: action.isPayPending };

    // Buscando arquivo
    case FILE_LOAD_PENDING:
      return { ...state, isFileLoadPending: action.isFileLoadPending };
    case FILE_LOAD_SUCCESS:
      return { ...state, isFileLoadSuccess: action.isFileLoadSuccess };
    case FILE_LOAD_ERROR:
      return { ...state, isFileLoadError: action.isFileLoadError };
    case FILE_LOAD_DATA:
      return { ...state, fileLoadData: action.fileLoadData };

    // Salvando arquivo
    case FILE_SAVE_PENDING:
      return { ...state, isFileSavePending: action.isFileSavePending };
    case FILE_SAVE_SUCCESS:
      return { ...state, isFileSaveSuccess: action.isFileSaveSuccess };
    case FILE_SAVE_ERROR:
      return { ...state, isFileSaveError: action.isFileSaveError };

    // Valor default
    default:
      return state;
  }
};

export default reducers;
