import {
    SET_LOGIN_PENDING,
    SET_LOGIN_SUCCESS,
    SET_LOGIN_ERROR,
    SET_SENHA_PENDING,
    SET_SENHA_SUCCESS,
    SET_SENHA_ERROR,
    SET_CAD_PENDING,
    SET_CAD_SUCCESS,
    SET_CAD_ERROR
} from '../actions/types';

const INITIAL_STATE = {
    isLoginSuccess: false,
    isLoginPending: false,
    loginError: null,
    isSenhaSuccess: false,
    isSenhaPending: false,
    senhaError: null,
    isCadPending: false,
    isCadSuccess: false,
    cadError: null
};

const reducers = function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_LOGIN_PENDING:
            return { ...state, isLoginPending: action.isLoginPending }
        case SET_LOGIN_SUCCESS:
            return { ...state, isLoginSuccess: action.isLoginSuccess }
        case SET_LOGIN_ERROR:
            return { ...state, loginError: action.loginError }
        case SET_SENHA_PENDING:
            return { ...state, isSenhaPending: action.isSenhaPending }
        case SET_SENHA_SUCCESS:
            return { ...state, isSenhaSuccess: action.isSenhaSuccess }
        case SET_SENHA_ERROR:
            return { ...state, senhaError: action.senhaError }
        case SET_CAD_PENDING:
            return { ...state, isCadPending: action.isCadPending }
        case SET_CAD_SUCCESS:
            return { ...state, isCadSuccess: action.isCadSuccess }
        case SET_CAD_ERROR:
            return { ...state, cadError: action.cadError }
        default:
            return state;
    }
};

export default reducers;