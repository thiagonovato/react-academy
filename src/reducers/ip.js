import {
    LOAD_DATA_REQUEST,
    LOAD_DATA_ERROR,
    LOAD_DATA_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    isRequesting: false,
    isIpError: false,
    iPSuccess: [],
};

const reducers = function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case LOAD_DATA_REQUEST:
            return { ...state, isRequesting: action.isRequesting }
        case LOAD_DATA_ERROR:
            return { ...state, isIpError: action.isIpError }
        case LOAD_DATA_SUCCESS:
            return { ...state, iPSuccess: action.iPSuccess }
        default:
            return state;
    }
};

export default reducers;