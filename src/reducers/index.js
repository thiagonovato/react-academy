import { combineReducers } from 'redux';
import AuthStore from './auth';
import categorias from './categorias'
import documentos from './documentos'
import upload from './upload'
// Aqui abaixo você vai incluindo os outros reducers (separados por responsabilidadades ou domínio)
// ...

const reducers = combineReducers({
    // Este é o nome da store que armazenará os dados pertinentes a autenticação.
    // Ex: AuthStore.isLoginSuccess, AuthStore.isLoginPending, AuthStore.loginError, AuthStore.isRecSuccess, AuthStore.isRecPending, AuthStore.recError, ...
    // Esta store será injetada nos seus componentes de tela, através do método mapStateToProps
    AuthStore,
    categorias,
    documentos,
    upload
});

export default reducers;
