import {
    LOAD_DOCUMENTOS_PENDING,
    LOAD_DOCUMENTOS_ERROR,
    LOAD_DOCUMENTOS_SUCCESS,
    LOAD_DOCUMENTOS_DATA,
    SAVE_DOCUMENTOS_PENDING,
    SAVE_DOCUMENTOS_ERROR,
    SAVE_DOCUMENTOS_SUCCESS,
    LOAD_DOCUMENTO_PENDING,
    LOAD_DOCUMENTO_ERROR,
    LOAD_DOCUMENTO_SUCCESS,
    LOAD_DOCUMENTO_DATA,
    UPDATE_DOCUMENTO_PENDING,
    UPDATE_DOCUMENTO_ERROR,
    UPDATE_DOCUMENTO_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    isDocumentoPending: false,
    isDocumentoErro: false,
    isDocumentoSuccess: false,
    documentoData: [],
    isCreateDocumentoPending: false,
    isCreateDocumentoErro: false,
    isCreateDocumentoSuccess: false,
    isSearchDocumentoPending: false,
    isSearchDocumentoErro: false,
    isSearchDocumentoSuccess: false,
    documentoSearchData: [],
    isUpdateDocumentoPending: false,
    isUpdateDocumentoErro: false,
    isUpdateDocumentoSuccess: false

};

const reducers = function (state = INITIAL_STATE, action) {
    switch (action.type) {

        // Listando documentos
        case LOAD_DOCUMENTOS_PENDING:
            return { ...state, isDocumentoPending: action.isDocumentoPending }
        case LOAD_DOCUMENTOS_ERROR:
            return { ...state, isDocumentoErro: action.isDocumentoErro }
        case LOAD_DOCUMENTOS_SUCCESS:
            return { ...state, isDocumentoSuccess: action.isDocumentoSuccess }
        case LOAD_DOCUMENTOS_DATA:
            return { ...state, documentoData: action.documentoData }

        // Editando documentos
        case SAVE_DOCUMENTOS_PENDING:
            return { ...state, isCreateDocumentoPending: action.isCreateDocumentoPending }
        case SAVE_DOCUMENTOS_ERROR:
            return { ...state, isCreateDocumentoErro: action.isCreateDocumentoErro }
        case SAVE_DOCUMENTOS_SUCCESS:
            return { ...state, isCreateDocumentoSuccess: action.isCreateDocumentoSuccess }

        // Buscando 1 documento
        case LOAD_DOCUMENTO_PENDING:
            return { ...state, isSearchDocumentoPending: action.isSearchDocumentoPending }
        case LOAD_DOCUMENTO_ERROR:
            return { ...state, isSearchDocumentoErro: action.isSearchDocumentoErro }
        case LOAD_DOCUMENTO_SUCCESS:
            return { ...state, isSearchDocumentoSuccess: action.isSearchDocumentoSuccess }
        case LOAD_DOCUMENTO_DATA:
            return { ...state, documentoSearchData: action.documentoSearchData }

        // Update documento
        case UPDATE_DOCUMENTO_PENDING:
            return { ...state, isUpdateDocumentoPending: action.isUpdateDocumentoPending }
        case UPDATE_DOCUMENTO_ERROR:
            return { ...state, isUpdateDocumentoErro: action.isUpdateDocumentoErro }
        case UPDATE_DOCUMENTO_SUCCESS:
            return { ...state, isUpdateDocumentoSuccess: action.isUpdateDocumentoSuccess }

        // Valor default
        default:
            return state;
    }
};

export default reducers;