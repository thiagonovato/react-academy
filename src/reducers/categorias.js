import {
    LOAD_CATEGORIAS_PENDING,
    LOAD_CATEGORIAS_ERROR,
    LOAD_CATEGORIAS_SUCCESS,
    LOAD_CATEGORIAS_DATA,
    SAVE_CATEGORIAS_PENDING,
    SAVE_CATEGORIAS_ERROR,
    SAVE_CATEGORIAS_SUCCESS,
    LOAD_CATEGORIA_PENDING,
    LOAD_CATEGORIA_ERROR,
    LOAD_CATEGORIA_SUCCESS,
    LOAD_CATEGORIA_DATA,
    UPDATE_CATEGORIA_PENDING,
    UPDATE_CATEGORIA_ERROR,
    UPDATE_CATEGORIA_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    isCategoriaPending: false,
    isCategoriaErro: false,
    isCategoriaSuccess: false,
    categoriaData: [],
    isCreateCategoriaPending: false,
    isCreateCategoriaErro: false,
    isCreateCategoriaSuccess: false,
    isSearchCategoriaPending: false,
    isSearchCategoriaErro: false,
    isSearchCategoriaSuccess: false,
    categoriaSearchData: [],
    isUpdateCategoriaPending: false,
    isUpdateCategoriaErro: false,
    isUpdateCategoriaSuccess: false

};

const reducers = function (state = INITIAL_STATE, action) {
    switch (action.type) {

        // Listando categorias
        case LOAD_CATEGORIAS_PENDING:
            return { ...state, isCategoriaPending: action.isCategoriaPending }
        case LOAD_CATEGORIAS_ERROR:
            return { ...state, isCategoriaErro: action.isCategoriaErro }
        case LOAD_CATEGORIAS_SUCCESS:
            return { ...state, isCategoriaSuccess: action.isCategoriaSuccess }
        case LOAD_CATEGORIAS_DATA:
            return { ...state, categoriaData: action.categoriaData }

        // Editando categorias
        case SAVE_CATEGORIAS_PENDING:
            return { ...state, isCreateCategoriaPending: action.isCreateCategoriaPending }
        case SAVE_CATEGORIAS_ERROR:
            return { ...state, isCreateCategoriaErro: action.isCreateCategoriaErro }
        case SAVE_CATEGORIAS_SUCCESS:
            return { ...state, isCreateCategoriaSuccess: action.isCreateCategoriaSuccess }

        // Buscando 1 categoria
        case LOAD_CATEGORIA_PENDING:
            return { ...state, isSearchCategoriaPending: action.isSearchCategoriaPending }
        case LOAD_CATEGORIA_ERROR:
            return { ...state, isSearchCategoriaErro: action.isSearchCategoriaErro }
        case LOAD_CATEGORIA_SUCCESS:
            return { ...state, isSearchCategoriaSuccess: action.isSearchCategoriaSuccess }
        case LOAD_CATEGORIA_DATA:
            return { ...state, categoriaSearchData: action.categoriaSearchData }

        // Update categoria
        case UPDATE_CATEGORIA_PENDING:
            return { ...state, isUpdateCategoriaPending: action.isUpdateCategoriaPending }
        case UPDATE_CATEGORIA_ERROR:
            return { ...state, isUpdateCategoriaErro: action.isUpdateCategoriaErro }
        case UPDATE_CATEGORIA_SUCCESS:
            return { ...state, isUpdateCategoriaSuccess: action.isUpdateCategoriaSuccess }

        // Valor default
        default:
            return state;
    }
};

export default reducers;